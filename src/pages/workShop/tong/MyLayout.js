import * as React from "react";
import NavBar from "src/components/Tong/Navbar";
import Footer from "src/components/Tong/Footer";

const MyLayout = ({ children }) => {
  return (
    <>
      <NavBar />
      <main>{children}</main>
      <Footer />
    </>
  );
};

export default MyLayout;
