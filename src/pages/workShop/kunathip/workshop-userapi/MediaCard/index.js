import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import { Typography } from "@mui/material";
import Button from "@mui/material/Button";
import Paper from "@mui/material/Paper";
import { useState, useEffect } from "react";
import TableContainer from "@mui/material/TableContainer";
import Avatar from "@mui/material/Avatar";
import Link from "@mui/material/Link";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Navbar from "src/components/Por/Navbar";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import TextField from "@mui/material/TextField";
import { id } from "date-fns/locale";

export default function MediaCard() {
  // ส่วนของ State
  const [items, setItems] = useState([]); // เก็บข้อมูลผู้ใช้
  const [selectedUser, setSelectedUser] = useState(null); // เก็บข้อมูลผู้ใช้ที่ถูกเลือก
  const [openEditDialog, setOpenEditDialog] = useState(false); // เก็บสถานะการเปิด/ปิด Dialog สำหรับการแก้ไขข้อมูลผู้ใช้

  // ฟังก์ชัน useEffect ทำงานเมื่อคอมโพเนนต์โดยมีไว้สำหรับดึงข้อมูลผู้ใช้เมื่อคอมโพเนนต์ถูกโหลด
  useEffect(() => {
    UserGet();
  }, []);

  // ฟังก์ชัน UserGet เพื่อดึงข้อมูลผู้ใช้จาก API
  const UserGet = () => {
    console.log("Fetching user data...");
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users`)
      .then((res) => {
        console.log("Response status:", res.status);
        return res.json();
      })
      .then((result) => {
        console.log("User data received:", result);
        setItems(result); // อัพเดทข้อมูลผู้ใช้ใน State
      })
      .catch((error) => {
        console.log("Error fetching user data:", error);
      });
  };

  // ฟังก์ชัน UserUpdate เพื่อเตรียมข้อมูลผู้ใช้ที่จะถูกแก้ไขและเปิด Dialog สำหรับการแก้ไข
  const UserUpdate = (user) => {
    setSelectedUser(user); // อัพเดทข้อมูลผู้ใช้ที่ถูกเลือก
    setOpenEditDialog(true); // เปิด Dialog สำหรับการแก้ไข
  };

  // ฟังก์ชัน handleUpdate เพื่อส่งข้อมูลการแก้ไขผู้ใช้ไปยัง API
  const handleUpdate = () => {
    console.log("Updating user:", selectedUser);

    const requestBody = {
      id: selectedUser.id,
      fname: selectedUser.fname,
      lname: selectedUser.lname,
    };

    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/update/${selectedUser.id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(requestBody),
    })
      .then((response) => response.json())
      .then((result) => {
        console.log("Update User Result:", result);

        if (result.status === "ok") {
          console.log("User updated successfully");
          alert(result.message);
        } else {
          console.log("User update failed");
          alert(result.message);
        }
      })
      .catch((error) => {
        console.error("Update User Error:", error);
        alert("Failed to update user");
      })
      .finally(() => {
        setOpenEditDialog(false); // ปิด Dialog สำหรับการแก้ไข
        UserGet(); // ดึงข้อมูลผู้ใช้ใหม่หลังจากการแก้ไข
      });
  };

  // ฟังก์ชัน handleCloseDialog เพื่อปิด Dialog สำหรับการแก้ไข
  const handleCloseDialog = () => {
    setOpenEditDialog(false);
  };

  // ฟังก์ชัน UserDelete เพื่อลบข้อมูลผู้ใช้
  const UserDelete = (id) => {
    console.log("Deleting user with ID:", id);

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      id: id,
    });

    var requestOptions = {
      method: "DELETE",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/delete`, requestOptions)
      .then((response) => {
        console.log("Delete User Response Status:", response.status);
        return response.json();
      })
      .then((result) => {
        console.log("Delete User Result:", result);
        alert(result["message"]);
        if (result["status"] === "ok") {
          console.log("User deleted successfully");
          UserGet(); // ดึงข้อมูลผู้ใช้ใหม่หลังจากการลบ
        } else {
          console.log("User deletion failed");
        }
      })
      .catch((error) => {
        console.log("Delete User Error:", error);
      });
  };

  return (
    <>
      <Navbar />
      <React.Fragment>
        <CssBaseline />
        <Container maxWidth="lg" sx={{ p: 2 }}>
          <Paper sx={{ p: 2 }}>
            <Box display="flex">
              <Box sx={{ flexGrow: 1 }}>
                <Typography variant="h6" gutterBottom component="div">
                  Users
                </Typography>
              </Box>
              <Box>
                <Link href="UserCreate">
                  <Button className="button" sx={{ backgroundColor: "#19a7ce", color: "white" }}>
                    Create
                  </Button>
                </Link>
              </Box>
            </Box>
            <TableContainer component={Paper} sx={{ m: 0 }}>
              <Box
                sx={{
                  display: "grid",
                  gridTemplateColumns: "repeat(auto-fill, minmax(320px, 1fr))",
                  gap: "70px",
                  backgroundColor: "e1e1e1",
                }}
              >
                {items.map((row) => (
                  <Card sx={{ maxWidth: 345 }} className="card" key={row.id}>
                    <CardMedia sx={{ height: 140 }} className="avatar">
                      <Avatar
                        sx={{
                          height: 180,
                          width: 180,
                          margin: 10,
                          display: "block",
                        }}
                        alt={row.username}
                        src={row.avatar}
                      />
                    </CardMedia>
                    <CardContent key={row.id} className="card-content">
                      <Typography
                        gutterBottom
                        variant="h5"
                        component="div"
                        className="id"
                        sx={{
                          fontWeight: "blod",
                          color: "#19a7ce",
                          backgroundColor: "#e5e5e5",
                          borderRadius: "5px",
                        }}
                      >
                        ID:{row.id}
                      </Typography>
                      <Typography variant="body2" color="text.secondary">
                        First Name: {row.fname} <br />
                        Last Name: {row.lname} <br />
                        Username: {row.username} <br />
                      </Typography>
                    </CardContent>
                    <CardActions className="btn-group">
                      <Button
                        className="button"
                        sx={{ backgroundColor: "#19a7ce", color: "white" }}
                        onClick={() => UserUpdate(row.id)}
                      >
                        Edit
                      </Button>
                      <Button
                        className="button"
                        sx={{ backgroundColor: "#19a7ce", color: "white" }}
                        onClick={() => UserDelete(row.id)}
                      >
                        Delete
                      </Button>
                    </CardActions>
                  </Card>
                ))}
              </Box>
            </TableContainer>
          </Paper>
        </Container>
      </React.Fragment>

      <Dialog open={openEditDialog} onClose={handleCloseDialog}>
        <DialogTitle>Edit User</DialogTitle>
        <DialogContent>
          <TextField
            label="First Name"
            value={selectedUser?.fname}
            onChange={(e) => setSelectedUser({ ...selectedUser, fname: e.target.value })}
            fullWidth
          />
          <TextField
            label="Last Name"
            value={selectedUser?.lname}
            onChange={(e) => setSelectedUser({ ...selectedUser, lname: e.target.value })}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDialog}>Cancel</Button>
          <Button onClick={handleUpdate} sx={{ backgroundColor: "#19a7ce", color: "white" }}>
            Update
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
