import React, { useState, useEffect } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  Badge,
  List,
  Grid,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Typography,
  Paper,
  TextField,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import RemoveIcon from "@mui/icons-material/Remove";
import AddIcon from "@mui/icons-material/Add";
import CategoriesFilter from "../CategoriesFilter";
import PriceFilter from "../PriceFilter";
const ProductList = ({ products, onAddToCart, searchText, selectedCategory, sortBy }) => {
  const filteredProducts = products
    .filter(
      (product) =>
        (searchText === "" || product.title.toLowerCase().includes(searchText.toLowerCase())) &&
        (selectedCategory === "" || product.category === selectedCategory)
    )
    .sort((a, b) => {
      switch (sortBy) {
        case "price-low-to-high":
          return a.price - b.price;
        case "price-high-to-low":
          return b.price - a.price;
        default:
          return 0;
      }
    });

  return (
    <List
      style={{ display: "flex", flexWrap: "wrap", justifyContent: "center", alignItems: "center" }}
    >
      {filteredProducts.map((product) => (
        <Paper
          key={product.id}
          style={{ flex: "0 0 25%", maxWidth: "25%", margin: "8px", position: "relative" }}
        >
          <ListItem style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
            <img
              src={product.image}
              alt={product.title}
              style={{ marginBottom: "10px", objectFit: "cover", maxHeight: "150px" }}
            />
            <IconButton
              onClick={() => {
                console.log(`Adding to cart: ${product.title}`);
                onAddToCart(product);
              }}
              style={{ position: "absolute", bottom: 0, right: 0 }}
            >
              <AddShoppingCartIcon />
            </IconButton>
            <ListItemText
              primary={product.title}
              secondary={`Price: $${product.price} | Discount: ${product.discount}%`}
              style={{ textAlign: "center", marginTop: "10px" }}
            />
          </ListItem>
        </Paper>
      ))}
    </List>
  );
};

const getImageUrl = (image) => {
  return image;
};
const ShoppingCart = ({ cartItems, onRemoveFromCart, onIncreaseQuantity, onDecreaseQuantity }) => {
  return (
    <List>
      {cartItems.map((item) => {
        const imageUrl = getImageUrl(item.image);
        console.log("Item in cart:", item); //log เช็คเฉยๆคับตรงนี้ไม่มีอะไรในก่อไผ่
        console.log("Image URL:", imageUrl); //log เช็คเฉยๆคับตรงนี้ไม่มีอะไรในก่อไผ่
        return (
          <ListItem key={item.id} sx={{ display: "grid", gridTemplateColumns: "1fr 2fr 1fr" }}>
            <img
              src={imageUrl}
              style={{ width: "50px", height: "auto", marginRight: "10px", objectFit: "cover" }}
            />
            <ListItemText
              primary={item.title}
              secondary={`Price: $${item.price} | Quantity: ${item.quantity}`}
            />
            <ListItemSecondaryAction sx={{ display: "flex", gap: 2, justifyContent: "flex-end" }}>
              <IconButton onClick={() => onDecreaseQuantity(item)}>
                <RemoveIcon />
              </IconButton>
              <IconButton onClick={() => onIncreaseQuantity(item)}>
                <AddIcon />
              </IconButton>
              <IconButton onClick={() => onRemoveFromCart(item)}>
                <DeleteIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        );
      })}
    </List>
  );
};

const OrderPage = () => {
  const [cart, setCart] = useState([]);
  const [openDialog, setOpenDialog] = useState(false);
  const [products, setProducts] = useState([]);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [quantityToAdd, setQuantityToAdd] = useState(1);
  const [dialogContentText, setDialogContentText] = useState("");
  const [searchText, setSearchText] = useState("");
  const [selectedCategory, setSelectedCategory] = useState("");
  const [sortBy, setSortBy] = useState("default");
  const uniqueCategories = [...new Set(products.map((product) => product.category))];
  const categories = uniqueCategories.filter((category) => category !== "");
  const [coupon, setCoupon] = useState("");
  const [couponDiscount, setCouponDiscount] = useState(0);

  useEffect(() => {
    fetch("https://fakestoreapi.com/products")
      .then((res) => res.json())
      .then((products) => {
        setProducts(products);
        console.log("Products fetched successfully:", products);
      })
      .catch((error) => console.error("Error fetching products:", error));
  }, []);

  const applyCoupon = () => {
    // ตรวจสอบรหัสคูปองและทำงานตามที่ต้องการ
    console.log("Applying coupon:", coupon);

    // เช็คคูปองและกำหนดส่วนลดที่เหมาะสม
    switch (coupon.toLowerCase()) {
      case "xyx":
        setCouponDiscount(5);
        break; // Add break statement
      case "xyz1":
        setCouponDiscount(10);
        break; // Add break statement
      default:
        setCouponDiscount(0);
    }
    
  };
  const handleAddToCart = (product) => {
    setSelectedProduct(product);
    setQuantityToAdd(1);
    setOpenDialog(true);
  };

  const handleAddToCartConfirm = () => {
    if (selectedProduct && quantityToAdd > 0) {
      console.log(
        `Adding to cart confirmed: ${selectedProduct.title} - Quantity: ${quantityToAdd}`
      );
      const existingItem = cart.find((item) => item.id === selectedProduct.id);

      if (existingItem) {
        setCart((prevCart) =>
          prevCart.map((item) =>
            item.id === selectedProduct.id
              ? { ...item, quantity: item.quantity + quantityToAdd }
              : item
          )
        );
      } else {
        setCart((prevCart) => [...prevCart, { ...selectedProduct, quantity: quantityToAdd }]);
      }

      console.log("Item added to cart:", selectedProduct);
      setOpenDialog(false);
    }
  };

  const handleRemoveFromCart = (itemToRemove) => {
    console.log(`Removing from cart: ${itemToRemove.title}`);
    setCart((prevCart) => prevCart.filter((item) => item.id !== itemToRemove.id));

    console.log("Item removed from cart:", itemToRemove);
  };
  const handleIncreaseQuantity = (item) => {
    console.log(`Increasing quantity of ${item.title}`);
    setCart((prevCart) =>
      prevCart.map((cartItem) =>
        cartItem.id === item.id ? { ...cartItem, quantity: cartItem.quantity + 1 } : cartItem
      )
    );
  };

  const handleDecreaseQuantity = (item) => {
    console.log(`Increasing quantity of ${item.title}`);
    setCart((prevCart) =>
      prevCart.map((cartItem) =>
        cartItem.id === item.id && cartItem.quantity > 1
          ? { ...cartItem, quantity: cartItem.quantity - 1 }
          : cartItem
      )
    );
  };

  const handleOpenDialog = () => {
    setOpenDialog(true);

    const totalAmount = calculateTotalAmount();
    const cartItemsInfo = cart
      .map(
        (item) => `${item.title} x ${item.quantity} - $${(item.price * item.quantity).toFixed(2)}`
      )
      .join("\n");

    setSelectedProduct(null);
    setQuantityToAdd(1);
    setDialogContentText(`Total: $${totalAmount.toFixed(2)}\n\n${cartItemsInfo}`);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  const calculateTotalAmount = () => {
    return cart.reduce((total, item) => total + item.price * item.quantity, 0);
  };
  const calculateDiscountedPrice = (price, discount) => {
    const discountAmount = (price * discount) / 100;
    return price - discountAmount;
  };

  

  return (
    <>
      <div
        style={{
          border: "2px solid red",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Typography variant="h4" gutterBottom sx={{ marginTop: "1rem" }}>
          สินค้าทั้งหมด
        </Typography>
        <TextField
          label="Search"
          variant="outlined"
          value={searchText}
          onChange={(e) => setSearchText(e.target.value)}
          style={{ marginBottom: "1rem" }}
        />
        <CategoriesFilter
          categories={categories}
          selectedCategory={selectedCategory}
          setSelectedCategory={setSelectedCategory}
        />
        <PriceFilter sortBy={sortBy} setSortBy={setSortBy} />
        <IconButton
          color="primary"
          onClick={() => {
            console.log("Opening cart dialog");
            handleOpenDialog();
          }}
          style={{ position: "absolute", marginTop: "10px", right: "2rem" }}
        >
          <Badge badgeContent={cart.length} color="error">
            <ShoppingCartIcon />
          </Badge>
        </IconButton>
        <ProductList
          products={products}
          onAddToCart={handleAddToCart}
          searchText={searchText}
          selectedCategory={selectedCategory}
          sortBy={sortBy}
          categories={categories} 
        />
      </div>

      <Dialog open={openDialog} onClose={handleCloseDialog}>
        <DialogTitle>Your Shopping Cart</DialogTitle>
        <DialogContent>
          {selectedProduct && (
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Typography variant="body1">{`เพิ่ม ${selectedProduct.title} ในตะกร้าของคุณ`}</Typography>
              </Grid>
              <Grid item xs={12} sx={{ display: "colum", gap: 1 }}>
                <TextField
                  label="Quantity"
                  type="number"
                  InputProps={{ inputProps: { min: 1 } }}
                  value={quantityToAdd}
                  onChange={(e) => setQuantityToAdd(Math.max(1, parseInt(e.target.value) || 0))}
                />

                <Button variant="contained" color="primary" onClick={handleAddToCartConfirm}>
                  Add to Cart
                </Button>
                <Button variant="outlined" color="secondary" onClick={handleCloseDialog}>
                  Cancel
                </Button>
              </Grid>
            </Grid>
          )}

          {cart.length > 0 && (
            <div>
              <TextField
                label="Coupon"
                variant="outlined"
                value={coupon}
                onChange={(e) => setCoupon(e.target.value)}
                style={{ marginBottom: "1rem" }}
              />
              <Button variant="contained" color="primary" onClick={applyCoupon}>
                Apply Coupon
              </Button>
              <ShoppingCart
                cartItems={cart}
                onRemoveFromCart={handleRemoveFromCart}
                onIncreaseQuantity={handleIncreaseQuantity}
                onDecreaseQuantity={handleDecreaseQuantity}
              />
              <Typography variant="h6" component="div">
                Total: ${calculateTotalAmount().toFixed(2)}
              </Typography>
            </div>
          )}
        </DialogContent>
      </Dialog>
    </>
  );
};

export default OrderPage;
