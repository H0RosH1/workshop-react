// นำเข้า React และ Hook ที่ใช้
import React, { useState } from "react";

// นำเข้า Layout และ Component ที่ต้องใช้
import { Layout } from "src/layouts/dashboard/layout";
import Footer from "src/components/Por/Footer";
import Box from "@mui/material/Box";
import { styled } from "@mui/system";
import { Button, Container, Grid, Paper, Typography } from "@mui/material";
import Navbar from "src/components/Por/Navbar";

// Styled Components สำหรับกำหนดสไตล์
const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
  margin: "10em 10em 10em 10em",
}));

const CalculatorContainer = styled(Container)({
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  border: "2px solid #427D9D",
  width: "350px",
  borderRadius: "10px",
  boxShadow: "5px 10px 10px #888888",
  backgroundColor: "#427D9D",
  color: "#FFFFFF",
  padding: "1em",
  marginTop: "4em",
  marginBottom: "4em",
});

const Input = styled("input")({
  width: "100%",
  boxSizing: "border-box",
  fontSize: "1.5em",
  padding: "0.5em",
  margin: "1em 0",
  borderRadius: "5px",
  border: "none",
  backgroundColor: "#1A2027",
  color: "#FFFFFF",
  textAlign: "right",
  outline: "none",
});

const ButtonGrid = styled(Grid)({
  display: "grid",
  gridTemplateColumns: "repeat(4, 1fr)",
  gap: "8px",
  marginTop: "1em",
});

const RoundButton = styled(Button)(({ theme }) => ({
  width: "50px",
  height: "50px",
  borderRadius: "50%",
  fontSize: "1.5em",
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#427D9D",
  color: "#FFFFFF",
  "&:hover": {
    backgroundColor: "#1A2027",
  },
}));

const kunathip = () => {
  // สถานะ state สำหรับเก็บผลลัพธ์และข้อมูลที่ใส่เข้ามา
  const [result, setResult] = useState("");
  const [input, setInput] = useState("");

  // ฟังก์ชันที่ใช้สำหรับการคลิกที่ปุ่ม
  const handleButtonClick = (value) => {
    setInput((prevInput) => prevInput + value);
  };

  // ฟังก์ชันที่ใช้สำหรับการล้างข้อมูล
  const handleClear = () => {
    setInput("");
    setResult("");
  };

  // ฟังก์ชันที่ใช้สำหรับการคำนวณผลลัพธ์
  const handleCalculate = () => {
    try {
      const calculatedResult = eval(input).toString();
      setResult(calculatedResult);
      console.log(`Calculation successful. Result: ${calculatedResult}`);
    } catch (error) {
      setResult("Error");
      console.error("Calculation error:", error);
    }
  };

  // ฟังก์ชันที่ใช้สำหรับการคำนวณเปอร์เซ็นต์
  const handlePercentage = () => {
    try {
      const percentageResult = (eval(input) / 100).toString();
      setResult(percentageResult);
      console.log(`Percentage calculation successful. Result: ${percentageResult}`);
    } catch (error) {
      setResult("Error");
      console.error("Percentage calculation error:", error);
    }
  };

  return (
    <>
      {/* Navbar component สำหรับการนำทาง */}
      <Navbar />

      {/* Box สำหรับการจัดรูปแบบและ Layout */}
      <Box sx={{ flexGrow: 1 }}>
        <div style={{ backgroundColor: "#FFF", margin: 0 }}>
          {/* Container สำหรับโปรแกรมคำนวณ */}
          <CalculatorContainer>
            {/* หัวข้อ */}
            <Typography variant="h4" gutterBottom>
              Calculator
            </Typography>

            {/* Input ที่แสดงผลลัพธ์ */}
            <Input type="text" value={input} readOnly />

            {/* ปุ่มที่ใช้ในการคำนวณ */}
            <ButtonGrid>
              {[7, 8, 9, "/"].map((value) => (
                <RoundButton key={value} onClick={() => handleButtonClick(value)}>
                  {value}
                </RoundButton>
              ))}
              {[4, 5, 6, "*"].map((value) => (
                <RoundButton key={value} onClick={() => handleButtonClick(value)}>
                  {value}
                </RoundButton>
              ))}
              {[1, 2, 3, "-"].map((value) => (
                <RoundButton key={value} onClick={() => handleButtonClick(value)}>
                  {value}
                </RoundButton>
              ))}
              {[0, "C", "=", "+", "%"].map((value) => (
                <RoundButton
                  key={value}
                  onClick={
                    value === "="
                      ? handleCalculate
                      : value === "C"
                      ? handleClear
                      : value === "%"
                      ? handlePercentage
                      : () => handleButtonClick(value)
                  }
                >
                  {value}
                </RoundButton>
              ))}
            </ButtonGrid>

            {/* แสดงผลลัพธ์ */}
            <Typography variant="body1" gutterBottom>
              Result: {result}
            </Typography>
          </CalculatorContainer>
        </div>
      </Box>

      {/* Footer สำหรับต่อท้าย */}
      <Footer />
    </>
  );
};

// กำหนด Layout ที่ใช้
kunathip.getLayout = (kunathip) => <Layout>{kunathip}</Layout>;

// ส่ง Component ออกไปให้นำไปใช้
export default kunathip;
