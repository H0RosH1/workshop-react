import React, { useState, useEffect } from "react";
import { Link } from "next/router";
import { Container, Row, Col, Card, ListGroup, ListGroupItem } from "reactstrap";
import Navbar from "src/components/Por/Navbarworkshop2/Navbar";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import { styled } from "@mui/material/styles";
import { Typography } from "@mui/material";
import Footer from "src/components/Por/Footerworkshop2/Footer";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

const Blog = () => {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    fetch("/api/posts")
      .then((response) => response.json())
      .then((data) => setPosts(data));
  }, []);

  return (
    <>
      <div>
        <Navbar />
        <Grid
          container
          justifyContent="center"
          alignItems="center"
          sx={{
            backgroundColor: "#A9A9A9",
            marginTop: "6rem",
            
            margin: "0",
            minHeight: "400px",
            position: "relative",
          }}
        >
          <div
            style={{
              marginTop: "1em",
              marginLeft: "auto",
              marginRight: "auto",
              maxWidth: "100%",
              position: "absolute",
              left: 0,
              right: 0,
             
            }}
          >
            <Typography style={{ paddingLeft: "2em", zIndex: 1 }}>
              <img
                src="https://i.ibb.co/J39Dr6t/Green-Bold-Simple-Sportswear-Logo.png"
                alt="Image 1"
                style={{ width: "200px", borderRadius: "1px", position: "absolute" }}
              />
            </Typography>
            <Typography
              style={{
                color: "white",
                textAlign: "left",
                paddingLeft: "4.1em",
                paddingTop: "8em",
                whiteSpace: "pre-line",
                whiteSpace: "pre-wrap",
              }}
            >
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus
            </Typography>
            <Typography
              style={{
                color: "white",
                textAlign: "left",
                paddingLeft: "4.1em",
                paddingTop: "-1em",
                whiteSpace: "pre-line",
                whiteSpace: "pre-wrap",
              }}
            >
              nec ullamcorper mattis, pulvinar dapibus leo.
            </Typography>
          </div>
        </Grid>

        <Grid
          container
          justifyContent="center"
          alignItems="center"
          spacing={2}
          style={{
            textAlign: "center",
            margin: "auto",
            

            maxWidth: "100%",
          }}
        >
          <Grid
            item
            xs={4}
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "column",
             
            }}
          >
            <img
              src="https://placekitten.com/200/200"
              alt="Image 1"
              style={{ width: "50%", borderRadius: "0px" }}
            />

            <Typography
              variant="subtitle1"
              sx={{ textAlign: "center", fontSize: "18px", color: "black", fontWeight: "bold" }}
            >
              10 TRENDS HOME DECORATION For Best House
            </Typography>
            <Typography
              variant="subtitle2"
              sx={{ textAlign: "center", marginTop: "8px", fontSize: "16px", color: "black" }}
            >
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
              incididunt ut labore et dolore magna aliqua.
            </Typography>
          </Grid>

          <Grid
            item
            xs={4}
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "column",
             
            }}
          >
            <img
              src="https://placekitten.com/200/200"
              alt="Image 1"
              style={{ width: "50%", borderRadius: "0px" }}
            />

            <Typography
              variant="subtitle1"
              sx={{ textAlign: "center", fontSize: "18px", color: "black", fontWeight: "bold" }}
            >
              10 TRENDS HOME DECORATION For Best House
            </Typography>
            <Typography
              variant="subtitle2"
              sx={{ textAlign: "center", marginTop: "8px", fontSize: "16px", color: "black" }}
            >
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
              incididunt ut labore et dolore magna aliqua.
            </Typography>
          </Grid>

          <Grid
            item
            xs={4}
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "column",
              
            }}
          >
            <img
              src="https://placekitten.com/200/200"
              alt="Image 1"
              style={{ width: "50%", borderRadius: "0px" }}
            />

            <Typography
              variant="subtitle1"
              sx={{ textAlign: "center", fontSize: "18px", color: "black", fontWeight: "bold" }}
            >
              10 TRENDS HOME DECORATION For Best House
            </Typography>
            <Typography
              variant="subtitle2"
              sx={{ textAlign: "center", marginTop: "8px", fontSize: "16px", color: "black" }}
            >
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
              incididunt ut labore et dolore magna aliqua.
            </Typography>
          </Grid>
          <Grid
            item
            xs={4}
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "column",
             
            }}
          >
            <img
              src="https://placekitten.com/200/200"
              alt="Image 1"
              style={{ width: "50%", borderRadius: "0px" }}
            />

            <Typography
              variant="subtitle1"
              sx={{ textAlign: "center", fontSize: "18px", color: "black", fontWeight: "bold" }}
            >
              10 TRENDS HOME DECORATION For Best House
            </Typography>
            <Typography
              variant="subtitle2"
              sx={{ textAlign: "center", marginTop: "8px", fontSize: "16px", color: "black" }}
            >
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
              incididunt ut labore et dolore magna aliqua.
            </Typography>
          </Grid>
          <Grid
            item
            xs={4}
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "column",
             
            }}
          >
            <img
              src="https://placekitten.com/200/200"
              alt="Image 1"
              style={{ width: "50%", borderRadius: "0px" }}
            />

            <Typography
              variant="subtitle1"
              sx={{ textAlign: "center", fontSize: "18px", color: "black", fontWeight: "bold" }}
            >
              10 TRENDS HOME DECORATION For Best House
            </Typography>
            <Typography
              variant="subtitle2"
              sx={{ textAlign: "center", marginTop: "8px", fontSize: "16px", color: "black" }}
            >
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
              incididunt ut labore et dolore magna aliqua.
            </Typography>
          </Grid>
          <Grid
            item
            xs={4}
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "column",
            
            }}
          >
            <img
              src="https://placekitten.com/200/200"
              alt="Image 1"
              style={{ width: "50%", borderRadius: "0px" }}
            />

            <Typography
              variant="subtitle1"
              sx={{ textAlign: "center", fontSize: "18px", color: "black", fontWeight: "bold" }}
            >
              10 TRENDS HOME DECORATION For Best House
            </Typography>
            <Typography
              variant="subtitle2"
              sx={{ textAlign: "center", marginTop: "8px", fontSize: "16px", color: "black" }}
            >
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
              incididunt ut labore et dolore magna aliqua.
            </Typography>
          </Grid>

        </Grid>

        <Footer />
      </div>
    </>
  );
};

export default Blog;
