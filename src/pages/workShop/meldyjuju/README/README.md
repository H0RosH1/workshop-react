
# Documentation of hathaichanok's workshop

## Introduction
ยินดีต้อนรับสู่ workshop ของฉันที่สามารถดูผลงาน workshop ต่างๆที่ได้รับมอบหมายในระหว่างการฝึกฝน 3 สัปดาห์ที่ผ่านมา

## ข้อกำหนดเบื้องต้น
ก่อนที่จะติดตั้งโปรเจ็กต์ ตรวจสอบให้แน่ใจว่าคุณได้ติดตั้งสิ่งที่จำเป็นต้องมีต่อไปนี้บนระบบของคุณ:

- ระบบปฏิบัติการ: ระบบใดๆ ที่สามารถรัน Node.js และเครื่องมือที่เกี่ยวข้องได้
- Node.js: เวอร์ชัน 18.2.0 หรือใหม่กว่า คุณสามารถดาวน์โหลดได้จากเว็บไซต์อย่างเป็นทางการของ Node.js
- npm: ตัวจัดการแพ็คเกจ Node.js โดยทั่วไปจะถูกติดตั้งโดยอัตโนมัติด้วย Node.js

### Installation

 **1.การติดตั้ง Gitlab (https://gitlab.com/)**

   - ขั้นที่ 1 ต้องทำการ fork (https://gitlab.com/H0RosH1/workshop-react) 
            
       ปิดเว็บไซต์ gitlab ขึ้นมา
       นำทางไปยังพื้นที่เก็บข้อมูล: https://gitlab.com/H0RosH1/workshop-react
       คลิกปุ่ม "Fork" เพื่อสร้างสำเนาของพื้นที่เก็บข้อมูลภายใต้บัญชีของคุณ

   - ขั้นที่ 2 copy link เพื่อทำการ clone และ remote ไปในโฟลเดอร์ที่ต้องการแล้วพิมพ์คำสั่ง 
     git clone <link-to-github-remote-repo> 

         git clone https://gitlab.com/meldyjuju/workshop-react.git 
         git cd workshop-react (เพื่อนำทางไปยังไดเร็กทอรีที่โคลน)
         git remote https://gitlab.com/H0RosH1/workshop-react.git
    
  -  ขั้นที่ 3 ตรวจเช็คว่า remote ถูกต้องหรือไม่ ด้วยคำสั่ง git remote -v 

         git remote -v

  -  ขั้นที่ 4 สร้าง branch ของตัวเองด้วยคำสั่ง git branch <ชื่อbranch>
  
         git branch meldyjuju

  - ขั้นที่ 5 ไปยัง branch ที่สร้างด้วยคำสั่ง  git checkout <ชื่อbranch>
       
         git checkout meldyjuju
  - ขั้นที่ 6 แก้ไข code ได้ด้วยคำสั่ง
         
         code .

  - ขั้นที่ 7 เมื่อแก้ไข code เรียบร้อยต้องเพิ่มไฟล์ที่แก้ไขด้วยคำสั่ง git add เป็นการระบุว่าต้องการที่จะสร้างความเปลี่ยนแปลงไฟล์ไหน
         
         git add . 
   
  - ขั้นที่ 8 ยืนยันการเปลี่ยนแปลงจากการ add. ด้วยคำสั่ง git commit -m "คอมเม้นสิ่งที่อยากจะบอก"
         
         git commit -m "done workshop5"

  - ขั้นที่ 9 ส่งไฟล์ที่ commit แล้วเข้าสู่ remote repository ด้วยคำสั่ง git push origin <branchที่เราสร้างไว้>
         
         ตัวอย่าง git push origin meldyjuju

  - ขั้นที่ 10 การ create merge request

        ไปที่ repository ของคุณ on GitLab และ click ที่ "create merge request" แล้วก็ทำตามคำแนะนำเพื่อ create the merge request (MR)

  - ขั้นที่ 11 ให้รอพี่เลี้ยงหรือคนที่ตรวจ code ของเราทำการกด Approve แค่นี้ก็เป็นอันเสร็จเรียบร้อย

         กรณีโดน Rejeacted
         ต้อง re-check ว่าอัพเดตงานถึงวันล่าสุดหรือยังด้วยคำสั่ง
         git branch เพื่อเช็คว่าตัวเองอยู่ branch ไหนในขณะนั้น
         git branch main เพื่อกลับไปที่ branch หลัก 
         git pull upstream main เพื่อดึงข้อมูลที่อัพเดตแล้วมาเมื่อเสร็จเรียบร้อยสามารถย้อนกลับไปทำขั้นที่ 4


**2.Install project dependencies:**

        npm install

**3.Start the project**

        npm run dev

This project should now be accessible at [http://localhost:3000] (http://localhost:3000).

## Environment Variables

### NEXT_PUBLIC_API_URL

- **Description**: ตัวแปรนี้ใช้กำหนด URL ของ API ที่ถูกเรียกใช้ในโปรเจ็กต์ React นี้
- **Default Value**: ถ้ามีค่าเริ่มต้นที่ใช้งานได้, คุณสามารถระบุ URL ของ API ที่คาดว่าจะถูกใช้เป็นค่าเริ่มต้น

       ```env
       NEXT_PUBLIC_API_URL=https://www.melivecode.com/api

- **Example**: `https://www.melivecode.com/api` 

หากต้องการตั้งค่าตัวแปรนี้ ให้สร้างไฟล์ชื่อ `.env.local` in the root of your project แล้วก็เพิ่มบรรทัดต่อไปนี้
       
       ```env
       NEXT_PUBLIC_API_URL=https://www.melivecode.com/api

## Dependencies

- [MUI](https://mui.com/) version. (^5.14.18)
- [React](https://reactjs.org/) version. (^18.2.0)
- [Next.js](https://nextjs.org/) version. (^13.5.6)

## Frameworks

- [Material-UI (MUI)](https://mui.com/): เป็น framework React ui ที่ยอดนิยม
- [React](https://reactjs.org/): เป็นไลบรารี JavaScript สำหรับสร้างส่วนต่อประสานกับผู้ใช้
- [Next.js](https://nextjs.org/): เป็น Framework ของ React สำหรับสร้างเว็บแอปพลิเคชันที่แสดงผลฝั่งเซิร์ฟเวอร์และสร้างแบบ stable

## การแก้ปัญหา (Troubleshooting)

  ปัญหา: ไม่สามารถเริ่มโปรเจ็กต์ได้
    - วิธีแก้: ตรวจสอบให้แน่ใจว่าได้ติดตั้ง Node.js และ npm ให้ถูกต้อง, และทำการติดตั้ง dependencies ด้วยคำสั่ง npm install

  ปัญหา: Environment Variables ไม่ถูกต้อง
    - วิธีแก้: ตรวจสอบไฟล์ .env.local และแน่ใจว่าตัวแปรถูกต้องและมีค่าถูกต้อง.

## Workshop 2 - ออกแบบตาม figma 

**วัตถุประสงค์**

เป็น workshopที่ 2 เกี่ยวข้องกับการออกแบบและการใช้งานเว็บแอปพลิเคชันโดยอิงจาก Figma wireframe สำหรับเว็บไซต์การออกแบบตกแต่งภายในบ้าน

## Figma Wireframe

- [Figma wireframe](https://www.figma.com/file/JbV1tKPt8CZXQdgp5EghEd/Home-Interior-Design-Website-Wireframe-(Community)?type=design&node-id=0-1&mode=design&t=6SFVcbSrR6SI2WCY-0)

## ส่วนประกอบของ workshop2 มีดังนี้

**1. Error Page เป็นหน้าสำหรับการออกแบบ**
    - ที่อยู่ไฟล์ src/pages/workShop/meldyjuju/ui/index.js
    ประกอบด้วย Navbarui2 ที่เป็น Navbar และ Bodyerror ที่เป็นส่วนของหน้าเว็บที่แสดงเมื่อเกิดข้อผิดพลาด 404
    มี Box ที่ใช้กำหนดลำดับแสดงของ Footer2, Footer3, และ Footer4 และกำหนดตำแหน่งที่เหมาะสมในหน้าเว็บ
    มีการใช้ CSS-in-JS โดยใช้ sx property ของ MUI (marginX, marginBottom, position, top, left, bottom, right, transform, zIndex)

ซึ่ง components ทั้งหมด 5 อันดังนี้ (Navbarui2.js,Bodyerror.js, Footer2.js, Footer3.js, Footer4.js)

    ** 1.Navbarui2.js  **
        - ที่อยู่ไฟล์ (src/components/pang/navbarui2)
        - มี AppBar สำหรับแถบเมนูด้านบน และ มี Toolbar สำหรับจัดวางองค์ประกอบ
        - มี Logo (WidgetsIcon), ชื่อเว็บ (Typography), และปุ่ม Navigation สำหรับหลายส่วน
        - Menu dropdown ที่สามารถแสดงเมนู Home, Portfolio, Blog
        - ใช้ Material-UI components เช่น AppBar, Toolbar, IconButton, Menu, MenuItem เป็นต้น

        // วิธีการเรียกใช้ 
        import Narbarui2 from 'src/components/pang/navbarui2';
        const YourComponent = () => {
            return (
             <div>
               <Narbarui2 />
             </div>
            );
        };

    ** 2.Bodyerror.js **
        - ที่อยู่ไฟล์ (src/components/pang/Bodyerror)
        - Component Body นี้ทำหน้าที่แสดงหน้าเว็บที่เกิดข้อผิดพลาด 404 (Page Not Found) พร้อมกับข้อความและปุ่มสั่งซื้อ
        - มี Container ที่ใช้กำหนดความกว้างสูงของหน้า และกำหนดสีพื้นหลังด้วย CSS (backgroundColor)
        - ประกอบไปด้วยข้อความ "ERROR 404" ที่ใช้สีขาว ขนาดใหญ่ และหนา (bold) และอยู่ตรงกลางหน้าจอด้วย CSS (textAlign: "center")
        - มีข้อความเพิ่มเติม "PAGE NOT FOUND, PLEASE GO BACK" ที่ใช้สีขาว, ขนาดใหญ่, และตัวหนา พร้อมกับการกำหนดระยะห่างของตัวอักษรด้วย CSS (letterSpacing: 2)
        - มีปุ่ม "ORDER NOW" ที่มีสีพื้นหลังเทาเข้ม, ขนาดใหญ่, ตัวหนา, และมีความสูงและความกว้างที่กำหนด พร้อมกับระยะห่างด้านบน (marginTop)

        // วิธีการเรียกใช้
        import Bodyerror from 'src/components/pang/Bodyerror';
        const YourComponent = () => {
            return (
             <div>
               <Bodyerror />
             </div>
            );
        };        
        

    ** 3.Footer2.js **
        - ที่อยู่ไฟล์ (src/components/pang/Footer2)
        - มีหน้าที่แสดงส่วนของหน้าเว็บที่เน้นการเปลี่ยนแปลงตกแต่งภายในบ้าน (interior design)
        - มี Box ที่ใช้กำหนดความสูง, สีพื้นหลัง, และ padding ต่าง ๆ ด้วย CSS-in-JS ของ MUI
        - มีข้อความ "LET'S CHANGE YOUR OWN HOME INTERIOR DESIGN NOW" ที่ใช้สีขาว, ขนาดใหญ่, ตัวหนา, และมีการกำหนดระยะห่างและตำแหน่งด้วย CSS-in-JS ของ MUI
        - ใช้ Box ของ Material-UI สำหรับการจัดวาง
        - มีปุ่ม "CONTACT US" ที่เป็นปุ่มกด (contained button) ที่มีสีพื้นหลังดำ

        // วิธีการใช้ 
        import Footer2 from 'src/components/pang/Footer2';
        const YourComponent = () => {
            return (
             <div>
               <Footer2 />
             </div>
            );
        };     
        
    ** 4.Footer3.js **
        - ที่อยู่ไฟล์ (src/components/pang/Footer3)
        - มีหน้าที่แสดงรูปภาพ (avatars) ในรูปแบบของ Grid 1 แถวและ 1 คอลัมน์
        - ใช้ Grid จาก Material-UI สำหรับการจัดวางแบบกริดโดย grid item นี้ถูกกำหนดให้มีการแสดงผลเนื้อหาภายในตรงกลางด้วย CSS    (display: "flex", justifyContent: "center", paddingLeft: "120px")
        - มีการแสดงรูปภาพ 4 รูป ทุกรูป Avatar มี alt (alternative text), src (ที่อยู่ของรูปภาพ), และ sx (style) ที่กำหนดความกว้างและความสูงเท่ากับ 75px
    
        // วิธีการเรียกใช้
        import Footer3 from 'src/components/pang/Footer3';
        const YourComponent = () => {
            return (
             <div>
               <Footer3 />
             </div>
            );
        };    
        
    ** 5.Footer4.js **
        - ที่อยู่ไฟล์ (src/components/pang/Footer4)
        - เป็นส่วนของ Footer ที่แสดงข้อมูลต่างๆล่างสุด
        - มีการใช้ Grid จาก Material-UI เพื่อจัดวางแบบ grid ซึ่งมี 3 คอลัมน์คือ 
            - Information Column: มีข้อมูลที่เกี่ยวข้องกับข้อมูลรายละเอียดหรือข้อมูลทั่วไป
            - Navigation Column: มีลิสต์ของ navigation links ที่ใช้แสดงทิศทางหรือหน้าที่นำไปยังแต่ละส่วนของเว็บไซต์
            - Contact Column: มีข้อมูลที่เกี่ยวข้องกับการติดต่อ, ที่อยู่, และอีเมล
        - มี Typography ในแต่ละ column เพื่อแสดงหัวข้อของแต่ละกล่องข้อมูล
        - มีการใช้ ArrowForwardIosIcon สำหรับแสดงเครื่องหมายลูกศร

        // วิธีการเรียกใช้
        import Footer4 from 'src/components/pang/Footer4';
        const YourComponent = () => {
            return (
             <div>
               <Footer4 />
             </div>
            );
        }; 

**2. Blog page เป็นหน้าสำหรับการออกแบบ (src/pages/workShop/meldyjuju/blog/index.js)**

มี components ทั้งหมด 6 อันดังนี้  

    ** 1.Navbarui2.js **                
    ** 2.Footer2.js **            \\\\\\\ ทั้ง 4 components อันเดียวกันกับ Error page 
    ** 3.Footer3.js **                        
    ** 4.Footer4.js **

    ** 5.Blog.js ** 
        - ที่อยู่ของไฟล์ (src/components/pang/Blog)
        - ถูกออกแบบให้ใช้สำหรับแสดงเนื้อหาที่ประกอบไปด้วยข้อมูลแต่ละกล่อง ที่ประกอบไปด้วยรูปภาพ, ข้อความหัวข้อ, และเนื้อหา
        - มีขอบมีการยกขึ้น (elevation) เพื่อให้มีเงา
        - มีระยะห่างด้านบน(padding-top) เพื่อจัดให้ห่างจากส่วนบนของ container

        // วิธีการเรียกใช้ 
        import Blog from 'src/components/pang/Blog';
        const YourComponent = () => {
            return (
             <div>
               <Blog />
             </div>
            );
        }; 

    ** 6.Bodyblog.js **  
        - ที่อยู่ของไฟล์ (src/components/pang/Bodyblog)
        - ถูกออกแบบให้ใช้สำหรับแสดงเนื้อหาที่ประกอบไปด้วยข้อมูลแต่ละกล่อง ที่ประกอบไปด้วยรูปภาพ, ข้อความหัวข้อ, และเนื้อหา
        - มีขอบมีการยกขึ้น (elevation) เพื่อให้มีเงา
        - มีระยะห่างด้านบน(padding-top) เพื่อจัดให้ห่างจากส่วนบนของ container

        // วิธีการเรียกใช้
        import Bodyblog from 'src/components/pang/Bodyblog';

        const YourComponent = () => {
            return (
             <div>
               <Bodyblog />
             </div>
            );
        }; 

**3. Portfolio page เป็นหน้าสำหรับการออกแบบ (src/pages/workShop/meldyjuju/portfolio/index.js)**

      เพื่อแสดงหน้าที่ประกอบไปด้วย Navbar (Navbarui2), ส่วนเนื้อหาของ portfolio (Bodyportfolio, Bodyportfolio2, Bodyportfolio3), และ Footer (Footer2, Footer3, Footer4) มี Box ที่เป็น container ที่มี position: "relative" เพื่อกำหนดตำแหน่งและขนาดของ Footer รวมไปถึงแต่ละ Footer ถูกกำหนด position: "absolute" และ zIndex ต่างกันเพื่อให้แสดงผลแบบลำดับหน้า. Footer2 จะอยู่ที่ด้านหน้าสุด (zIndex: 3), Footer3 จะอยู่ต่อจาก Footer2 (zIndex: 2), และ Footer4 จะอยู่ที่ด้านหลังสุด (zIndex: 1)

มี components ทั้งหมด 6 อันดังนี้  

     ** 1.Navbarui2.js **                
     ** 2.Footer2.js **            \\\\\\\ ทั้ง 4 components อันเดียวกันกับ Error page 
     ** 3.Footer3.js **                        
     ** 4.Footer4.js **
 
    ** 5.bodyportfolio.js ** 
        - ที่อยู่ของไฟล์ (src/components/pang/bodyportfolio)
        - ถูกออกแบบมาเพื่อแสดงหน้าที่ของ Portfolio ที่ประกอบไปด้วยภาพแกลลอรี่และข้อความที่เกี่ยวข้อง
        - มีการใช้ Container จาก Material-UI เพื่อกำหนดลักษณะของ container ที่เก็บเนื้อหาของ Portfolio
        - Container นี้ถูกกำหนด maxWidth="full" เพื่อให้ container นี้เต็มหน้าจอ
        - มีการกำหนดสีพื้นหลังของ container เป็น "#00796b" (สีเขียวเข้ม)
        - Container นี้มีความสูง 30em, มีการแสดงผลเนื้อหาในลักษณะของคอลัมน์ (column) ด้วย flexDirection: "column"
        - มีการใช้ div สามตัวเพื่อแสดงข้อความที่เกี่ยวข้องกับ Portfolio
            - ข้อความ "PORTFOLIO" และ "GALLERY" มีขนาดตัวอักษร 50px, มีน้ำหนักตัวหนา (bold), และมีสีขาว
            - ข้อความ "Lorem ipsum dolor sit amet, consectetur adipiscing elit." และ "Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo." มีขนาดตัวอักษร 15px, สีขาว, และมีการจัดวางแนวนอนซ้าย (textAlign: "left")
            - นอกจากนี้, มีการใช้ whiteSpace: "nowrap" เพื่อป้องกันข้อความที่มีขนาดใหญ่จากการขึ้นบรรทัดใหม่.

        // วิธีการเรียกใช้ 
        import Blog from 'src/components/pang/bodyportfolio';
        const YourComponent = () => {
            return (
             <div>
               <Blog />
             </div>
            );
        }; 

    ** 6.bodyportfolio2.js ** 
        - ที่อยู่ของไฟล์ (src/components/pang/bodyportfolio2)
        - สร้างขึ้นเพื่อแสดงส่วนของ Portfolio ที่ประกอบไปด้วยวิดีโอ YouTube และข้อมูลต่างๆ
        - มีการใช้ Material-UI components เช่น Grid, Paper, Typography, Card, CardMedia, และ CheckCircleIcon
        - Grid container ที่ใช้ spacing={2} และ justifyContent="space-around" ทำให้เนื้อหาอยู่ในลักษณะกริดและมีการจัดวางในแนวนอนตามพื้นที่ที่ให้มีการเว้นระยะห่างของ 2 หน่วยระหว่าง Grid items
        - มี 2 Grid items โดยแต่ละตัวมีขนาดที่กำหนดไว้สำหรับขนาดจอที่ต่างกัน (xs, sm, md, lg).
              1. Grid item ที่ 1 (xs={12} sm={6} md={6} lg={6}):
                  มี Paper ที่มี elevation={3} (เงาของกระดาษ) และมีขนาดความสูง 400px
                  มี iframe ที่ใช้แสดงวิดีโอ YouTube
              2. Grid item ที่ 2 (xs={12} sm={6} md={6} lg={6}):
                  มี Paper ที่มี elevation={3} และมีขนาดความสูง 400px.
                  มี Typography ในรูปแบบต่าง ๆ สำหรับแสดงข้อความ
                  มีรายการข้อความที่ใช้ CheckCircleIcon เพื่อแสดงข้อมูลเพิ่มเติม
          
        // วิธีการเรียกใช้ 
        import Blog from 'src/components/pang/bodyportfolio2';
        const YourComponent = () => {
            return (
             <div>
               <Blog />
             </div>
            );
        }; 

    ** 7.bodyportfolio3.js ** 
        - ที่อยู่ของไฟล์ (src/components/pang/bodyportfolio3)
        - สร้างขึ้นเพื่อแสดงส่วนของ Portfolio ที่ประกอบไปด้วยรูปภาพที่แสดงผลงานต่างๆ
        - CustomBox มีการใช้ Material-UI components เช่น Box, Grid, และ Paper 
        - Paper ใช้ elevation={3} (เงาของกระดาษ) และมีการกำหนด padding เพื่อสร้างพื้นที่ของกระดาษทำให้เนื้อหาอยู่ในลักษณะกริดและมีการจัดวางในแนวนอนตามพื้นที่ที่ให้มีการเว้นระยะห่างของ 2 หน่วยระหว่าง Grid items
        - ใน Grid container มี spacing={2} ทำให้ระยะห่างของ Grid items 
        - ในแต่ละ Grid item, มีการใช้ <img> เพื่อแสดงรูปภาพและมีการกำหนดความสูงและความกว้างของรูปภาพเพื่อให้แสดงผลได้อย่างเหมาะสม.
        - ข้อความ "PORTFOLIO" และ "OUR RECENT WORK" ถูกแสดงในรูปแบบของ <div> ที่กำหนดรูปแบบต่าง ๆ เพื่อสร้างการแสดงผลที่น่าสนใจ.

        // วิธีการเรียกใช้ 
        import Blog from 'src/components/pang/bodyportfolio3';
        const YourComponent = () => {
            return (
             <div>
               <Blog />
             </div>
            );
        }; 

## Workshop 3 - ระบบ Login

    ทำหน้าที่เป็นหน้าหลักและแสดง component Login โดยรับฟังก์ชัน callback (handleLogin) เพื่อรับ JWT token จาก component Login

**วัตถุประสงค์**

    1. สร้างระบบ login โดยใช้ API ที่ให้มา https://www.melivecode.com/api/login พร้อมทำการบันทึก log ของการทำงานระบบ login 
    2. สร้างไฟล์ .env เพื่อเก็บค่าต่าง ๆ ที่ใช้ในโค้ด รวมถึงเก็บ JWT token ลงใน local storage

**ข้อมูลสำคัญที่ต้องใช้สำหรับการ Login** 

     "username": "karn.yong@melivecode.com",
     "password": "melivecode

**1. คำอธิบายฟังก์ชันและ Component**

    ** 1.1 Login.js ** 
        - ที่อยู่ของไฟล์ (src/components/pang/Login)
        เป็น Component Login จัดการรับข้อมูลจากผู้ใช้เกี่ยวกับ username และ password และทำการ request ไปยัง API เพื่อ login พร้อมจัดการ JWT token และในส่วนของ style ใช้ Material-UI

    Props:
        onLogin (function): ฟังก์ชัน callback ที่ถูกเรียกเมื่อกระบวนการเข้าสู่ระบบสำเร็จ โดยจะได้รับโทเค็น JWT เป็นพารามิเตอร์

    การจัดการสถานะ:
        คอมโพเนนต์ใช้ useState hook เพื่อจัดการสถานะของตัวเอง ซึ่งประกอบด้วย
          -  username (string): สำหรับเก็บค่าชื่อผู้ใช้ที่ผูกกับ input field
          -  password (string): สำหรับเก็บค่ารหัสผ่านที่ผูกกับ input field

    Function handleLogin:
       - ทำหน้าที่ดำเนินการกระบวนการเข้าสู่ระบบ เมื่อมีการคลิกที่ปุ่ม "Login" จะทำการตรวจสอบข้อมูลที่ป้อน และส่งคำขอไปยัง API ของเซิร์ฟเวอร์
    
    การแสดง Log:
        - ข้อมูลการร้องขอ (info): แสดง URL และข้อมูลที่ถูกส่งไปยัง API
        - ข้อผิดพลาด (error): แสดงข้อความเมื่อข้อมูลที่จำเป็นไม่ถูกต้อง

    การทำงาน:
        - ตรวจสอบว่ามีรหัสผ่านและชื่อผู้ใช้หรือไม่
        - ส่งคำขอไปยัง API โดยใช้ fetch
        - ดึงข้อมูลจากการตอบรับและแปลงเป็น JSON
        - ถ้าเข้าสู่ระบบสำเร็จ:
            - แสดงข้อความ Login successful และเก็บ JWT ใน Local Storage
            - เรียกใช้ onLogin callback พร้อมกับ JWT
        - ถ้าเข้าสู่ระบบไม่สำเร็จ:
            - แสดงข้อความ Login failed

        // วิธีการเรียกใช้ 
        import Login from 'src/components/pang/Login';
        const YourComponent = () => {
            const handleLogin = (jwt) => {
               console.log('JWT:', jwt);       
             };

            return (
                <div>
                    <Login onLogin={handleLogin} />
                </div>
              );
            };
        export default YourComponent;

    ผ่าน onLogin callback เพื่อรับ JWT เมื่อมีการเข้าสู่ระบบสำเร็จ

  **2. การใช้ .env และการเก็บ JWT ใน Local Storage**

        2.1. .env
        ในการใช้ .env file ถ้ายังไม่มีสามารถสร้างไฟล์ .env ที่ root ของโปรเจคดังนี้

        NEXT_PUBLIC_API_URL=https://www.melivecode.com/api

        และเรียกใช้ในโค้ดของคุณโดย process.env.NEXT_PUBLIC_API_URL

        2.2. การเก็บ JWT ใน Local Storage
        ในกรณีที่เข้าสู่ระบบสำเร็จ คุณใช้ localStorage.setItem("jwt", jwt); เพื่อเก็บ JWT ใน Local Storage เพื่อให้สามารถนำมาใช้ใน request อื่น ๆ ในที่ต่อไป

        // วิธีการใช้ .env ในโค้ด
        const apiUrl = process.env.NEXT_PUBLIC_API_URL;
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/login`)
          .then(response => response.json())
          .then(data => console.log(data));

  **3. การดู Log ใน Console**

        เมื่อต้องการทดสอบโปรแกรม สามารถเปิด Developer Tools ในเว็บบราวเซอร์ (โดยกด F12 หรือ right-click แล้วเลือก "Inspect") และไปที่แท็บ "Console" เพื่อดู log ที่ด้ทำการ console ออกมา
        อ่าน log ที่แสดงเหตุการณ์การทำงานของระบบ login และคำขอไปยัง API เพื่อตรวจสอบว่าระบบทำงานถูกต้องหรือไม่


## Workshop 4 - ดึงข้อมูลผู้ใช้หลังจาก Login

**วัตถุประสงค์**

      - ใช้ JWT ที่ได้จากการเข้าสู่ระบบ (Workshop 3) ทำการดึงข้อมูลผู้ใช้จาก API https://www.melivecode.com/api/auth/user
      - แสดงข้อมูลผู้ใช้ที่ได้จาก API และเก็บข้อมูลนี้ลงใน Local Storage
      - บันทึก log ของการดึงข้อมูลและแสดงผล

  การใช้งานใน
      - handleLogin Function: ทำการเซ็ต JWT ที่ได้จากการ login เพื่อทำให้ Component UserInfo แสดงข้อมูลผู้ใช้
      - handleLogout Function: ทำการลบ JWT และข้อมูลผู้ใช้ที่เก็บไว้ใน Local Storage เมื่อ logout

  การแสดงผล:
      - แสดง Component UserInfo ถ้ามี JWT (ผู้ใช้ล็อกอิน)
      - แสดง Component Login ถ้าไม่มี JWT (ผู้ใช้ไม่ได้ล็อกอิน)  

 **ข้อมูลสำคัญที่ต้องใช้สำหรับการ Login** 

     "username": "karn.yong@melivecode.com",
     "password": "melivecode     

**1. คำอธิบาย Component**

    ** 1.1 UserInfo.js **

    - ที่อยู่ของไฟล์ src/components/pang/UserInfo

    - Props:
        jwt (string): JWT token ที่ใช้ในการทำ request ไปยัง API

    - userData (object): สำหรับเก็บข้อมูลผู้ใช้ที่ได้จาก API
    - useEffect: ใช้เพื่อดึงข้อมูลผู้ใช้เมื่อ Component ถูกโหลดหรือ JWT มีการเปลี่ยนแปลง
    - fetchUserData Function: ทำการ request ไปยัง API ด้วย JWT ที่กำหนดใน Header และเก็บข้อมูลผู้ใช้ที่ได้ลงใน State และ Local Storage

    - การแสดงผล:
        แสดงข้อมูลผู้ใช้ถ้ามีข้อมูล
        แสดงข้อความ "Loading user data..." ในกรณีที่กำลังโหลดข้อมูล


**2. การใช้ .env และการเก็บ JWT ใน Local Storage**

      - ใช้ .env file เพื่อเก็บ NEXT_PUBLIC_API_URL ที่เป็น URL ของ API

**3. การดู Log ใน Console**

      - เมื่อต้องการทดสอบโปรแกรม สามารถเปิด Developer Tools ในเว็บบราวเซอร์ (โดยกด F12 หรือ right-click แล้วเลือก "Inspect") 
      - ไปที่แท็บ "Console" เพื่อดู log ที่ทำการ console ออกมา
      - อ่าน log ที่แสดงเหตุการณ์การทำงานของระบบ login และการดึงข้อมูลผู้ใช้ เพื่อตรวจสอบว่าระบบทำงานถูกต้องหรือไม่


## Workshop5 การใช้งาน API แบบ Public 

      Workshop 5 นี้มุ่งเน้นการใช้งาน API ทั้งหมดผ่านปุ่มกดในแต่ละส่วนของแอปพลิเคชัน โดยใช้ https://www.melivecode.com/ เป็นตัวอย่าง API ที่ใช้งาน

**โดยมี components ที่ถูกแบ่งเป็นส่วนย่อยๆ ตามฟังก์ชันและการทำงานที่แตกต่างกันไป**

1.Api (workapi/index.js)

      - Component หลักที่รวมฟังก์ชันการเรียกใช้ API ทั้งหมดและปุ่มควบคุม
      - ประกอบไปด้วย Totalapi, Totalattractions, Petsale, Create, Update, Delete

2.Totalapi (components/pang/workshop5/totalapi.js)

      - ฟังก์ชันและปุ่มที่เกี่ยวข้องกับ API ที่ใช้ดึงข้อมูลผู้ใช้
      - รองรับการเรียกใช้ API ต่าง ๆ เช่น fetchUsers, fetchSearch, fetchPagination, fetchSort, fetchAll, fetchDetail

      2.1 fetchUsers()

          ฟังก์ชันนี้ทำหน้าที่ดึงข้อมูลผู้ใช้ทั้งหมดจาก API endpoint ${apiUrl}/users และนำข้อมูลที่ได้มาเก็บใน userList state variable

          * วิธีการใช้งาน
          ในส่วนของ UI สามารถใช้ปุ่ม "USER LIST" เพื่อเรียกใช้ fetchUsers โดยระบุไว้ใน onClick event
          <Button variant="contained" color="primary" onClick={fetchUsers} sx={{ margin: "10px" }}>
          USER LIST
          </Button>

          * ตัวอย่างการเรียกใช้ fetchUsers
          fetchUsers(); // เรียกใช้ฟังก์ชันเมื่อ UI ถูกโหลด

          * ข้อมูลที่ได้รับ
          console.log(userList); // แสดงข้อมูลผู้ใช้ทั้งหมด
          
      2.2 fetchSearch()

           ฟังก์ชันนี้ใช้เพื่อดึงข้อมูลผู้ใช้ที่ตรงกับคำค้นหา "karn" จาก API endpoint ${apiUrl}/users?search=karn และนำข้อมูลที่ได้มาเก็บใน userSearch state variable.

          * วิธีการใช้งาน
          ในส่วนของ UI สามารถใช้ปุ่ม "USER LIST (search)" เพื่อเรียกใช้ fetchSearch โดยระบุไว้ใน onClick event
          <Button variant="contained" color="primary" onClick={fetchSearch} sx={{ margin: "10px" }}>
          USER LIST (search)
          </Button>

          * ตัวอย่างการเรียกใช้  fetchSearch
          fetchSearch(); // เรียกใช้เมื่อต้องการค้นหาผู้ใช้

          * ข้อมูลที่ได้รับ
          console.log(userSearch); // แสดงข้อมูลผู้ใช้ที่ตรงกับคำค้นหา "karn"
     

      2.3 fetchPagination()

          ฟังก์ชันนี้ใช้เพื่อดึงข้อมูลผู้ใช้ที่อยู่ในหน้าที่ 1 และแสดงผล 10 รายการต่อหน้า จาก API endpoint ${apiUrl}/users?page=1&per_page=10 และนำข้อมูลที่ได้มาเก็บใน userPagination state variable.

          * วิธีการใช้งาน
          ในส่วนของ UI สามารถใช้ปุ่ม "USER LIST (pagination)" เพื่อเรียกใช้ fetchPagination โดยระบุไว้ใน onClick event
          <Button variant="contained" color="primary" onClick={fetchPagination} sx={{ margin: "10px" }}>
          USER LIST (search)
          </Button>

          * ตัวอย่างการเรียกใช้ fetchPagination
          fetchPagination(); // เรียกใช้เมื่อต้องการดึงข้อมูลผู้ใช้ที่อยู่ในหน้าที่ 1

          * ข้อมูลที่ได้รับ
          console.log(userPagination); // แสดงข้อมูลผู้ใช้ที่อยู่ในหน้าที่ 1

      2.4 fetchSort()

          ฟังก์ชันนี้ใช้เพื่อดึงข้อมูลผู้ใช้จาก API endpoint ${apiUrl}/users?sort_column=id&sort_order=desc โดยทำการเรียงลำดับตาม id ในลำดับที่มีลำดับน้อยไปหาลำดับมาก และจากนั้นนำข้อมูลที่ได้มาเก็บใน userSort state variable.

          * วิธีการใช้งาน
          ในส่วนของ UI สามารถใช้ปุ่ม "USER LIST (fetchSort)" เพื่อเรียกใช้ fetchSort โดยระบุไว้ใน onClick event
          <Button variant="contained" color="primary" onClick={fetchSort} sx={{ margin: "10px" }}>
          USER LIST (search)
          </Button>

          * ตัวอย่างการเรียกใช้ fetchSort
          fetchSort(); // เรียกใช้เมื่อต้องการดึงข้อมูลผู้ใช้ที่เรียงลำดับตาม id จากมากไปหาน้อย

          * ข้อมูลที่ได้รับ
          console.log(userSort); // แสดงข้อมูลผู้ใช้ที่ถูกเรียงลำดับ

      2.5 fetchAll()

          ฟังก์ชัน fetchAll นี้ทำการเรียกใช้ API endpoint ที่เป็น URL ${apiUrl}/users?search=ka&page=1&per_page=10&sort_column=id&sort_order=desc โดยส่งคำร้องขอแบบ GET ไปยังเว็บเซิร์ฟเวอร์ ซึ่งมีรายละเอียดการส่งคำร้องขอดังนี้:
            - ${apiUrl}/users: ระบุ API endpoint เพื่อดึงข้อมูลผู้ใช้ (users) จากเว็บเซิร์ฟเวอร์ที่ระบุในตัวแปร apiUrl.
            - search=ka: ระบุคำค้นหาที่ต้องการในการดึงข้อมูลผู้ใช้ (users) โดยในที่นี้คือ "ka".
            - page=1: ระบุหน้าที่ต้องการในการแบ่งหน้าข้อมูล, ในที่นี้คือหน้าที่ 1.
            - per_page=10: ระบุจำนวนรายการที่ต้องการให้แสดงต่อหน้า, ในที่นี้คือ 10 รายการ.
            - sort_column=id: ระบุให้ข้อมูลถูกเรียงตามคอลัมน์ id.
            - sort_order=desc: ระบุให้ข้อมูลถูกเรียงจากมากไปน้อย (descending order)

        คำอธิบายโค้ด

          const apiUrl = process.env.NEXT_PUBLIC_API_URL;: กำหนดตัวแปร apiUrl เพื่อเก็บ URL ของ API ที่ต้องการเรียกใช้ ซึ่งถูกนำมาจาก environment variable NEXT_PUBLIC_API_URL ที่ถูกกำหนดไว้ในโปรเจกต์

          const endpoint = ${apiUrl}/users?search=ka&page=1&per_page=10&sort_column=id&sort_order=desc;: สร้าง URL สำหรับ API endpoint โดยรวม parameter ต่างๆ ที่ใช้ในคำร้องขอ GET

          fetch(endpoint): ใช้ fetch เพื่อทำการส่งคำร้องขอ GET ไปยัง API endpoint ที่กำหนด

          .then((response) => response.json()): จากนั้นใช้ .then เพื่อดึงข้อมูล JSON จาก response ที่ได้จาก API

          .then((data) => { setUserlistAll(data); console.log("Userlist(all) เส้น5:", data); }): เมื่อได้ข้อมูล JSON จาก API ให้ทำการกำหนดข้อมูลลงใน state userlistAll และทำการ log ข้อมูลนั้นลงใน console

          .catch((error) => { console.error("Error Userlist(all):", error); }): กรณีที่มีข้อผิดพลาดในกระบวนการดึงข้อมูลจาก API ให้ทำการจัดการและ log ข้อผิดพลาดลงใน console

      2.6 fetchDetail()

          ฟังก์ชัน fetchDetail นี้ทำหน้าที่ดึงข้อมูลรายละเอียดของผู้ใช้ที่มี ID เท่ากับ 1 จาก API endpoint ${apiUrl}/users/1 และนำข้อมูลที่ได้มาเก็บในตัวแปร userDetail ที่เป็น state variable

          * วิธีการใช้งาน
          ในส่วนของ UI สามารถใช้ปุ่ม "USER DETAIL" เพื่อเรียกใช้ fetchDetail โดยระบุไว้ใน onClick event
          <Button variant="contained" color="primary" onClick={fetchDetail} sx={{ margin: "10px" }}>
          USER LIST (search)
          </Button>

          * ตัวอย่างการเรียกใช้ fetchDetail
          fetchDetail(); // เรียกใช้เมื่อต้องการดึงข้อมูลรายละเอียดของผู้ใช้ที่มี ID เท่ากับ 1

          * ข้อมูลที่ได้รับ
          console.log(userDetail); // แสดงข้อมูลรายละเอียดของผู้ใช้ที่มี ID เท่ากับ 1
         

3.Create (components/pang/workshop5/Create.js)

      - ฟอร์มและปุ่มสำหรับสร้างผู้ใช้
      - รองรับการเรียกใช้ API สำหรับการสร้างผู้ใช้ที่ส่งข้อมูลผ่าน HTTP POST

        คำอธิบายโค้ด

            State Hooks:
              - useState ถูกใช้เพื่อเก็บข้อมูลของผู้ใช้ที่ใส่เข้ามาในฟอร์ม เช่น ชื่อ, นามสกุล, ชื่อผู้ใช้, รหัสผ่าน, อีเมล, และลิงก์ Avatar
              - createLog ถูกใช้เพื่อเก็บข้อผิดพลาดหรือข้อความที่ต้องการแสดงหลังจากการสร้างผู้ใช้

            ธีม:
              - ใช้ createTheme และ ThemeProvider จาก Material-UI เพื่อกำหนดธีมสีของฟอร์ม.
    
          ฟังก์ชัน handleCreate:
              - ทำการสร้างผู้ใช้โดยส่งคำร้องขอ HTTP POST ไปที่ ${process.env.NEXT_PUBLIC_API_URL}/users/create.
              - ตรวจสอบการตอบกลับจากเซิร์ฟเวอร์ ถ้าเป็นไปตามที่คาดหวัง, จะ log ข้อมูลผู้ใช้ที่ได้และเรียก onCreate ส่งข้อมูล.
              - ถ้ามีข้อผิดพลาด, จะ log และแสดงข้อความผิดพลาด

          UI Components:
              - มี TextField สำหรับกรอกข้อมูลผู้ใช้ที่ต้องการสร้าง
              - มีปุ่ม "Create User" สำหรับเรียกใช้งานฟังก์ชัน handleCreate.


4.Update (components/pang/workshop5/Update.js)

      - ฟอร์มและปุ่มสำหรับอัปเดตข้อมูลผู้ใช้
      - รองรับการเรียกใช้ API สำหรับการอัปเดตข้อมูลผู้ใช้ที่ส่งข้อมูลผ่าน HTTP PUT

       คำอธิบายโค้ด

            State Hooks:
              - useState ถูกใช้เพื่อเก็บข้อมูลเลขประจำตัวประชาชนของผู้ใช้ (User ID) และนามสกุล (Last Name)
              - updateLog ถูกใช้เพื่อเก็บข้อผิดพลาดหรือข้อความที่ต้องการแสดงหลังจากการอัปเดตข้อมูล

            ธีม:
              - ใช้ createTheme และ ThemeProvider จาก Material-UI เพื่อกำหนดธีมสีของฟอร์ม
    
          ฟังก์ชัน handleCreate:
              - ทำการอัปเดตข้อมูลผู้ใช้โดยส่งคำร้องขอ HTTP PUT ไปที่ ${process.env.NEXT_PUBLIC_API_URL}/users/update
              - ตรวจสอบการตอบกลับจากเซิร์ฟเวอร์ ถ้าเป็นไปตามที่คาดหวัง, จะ log ข้อมูลผู้ใช้ที่ได้และเรียก onUpdate ส่งข้อมูล
              - ถ้ามีข้อผิดพลาด, จะ log และแสดงข้อความผิดพลาด

          UI Components:
              - TextField สำหรับกรอก User ID และนามสกุลที่ต้องการอัปเดต
              - ปุ่ม "Update User" สำหรับเรียกใช้งานฟังก์ชัน handleUpdate

5.Delete (components/pang/workshop5/Delete.js)

      - ฟอร์มและปุ่มสำหรับลบข้อมูลผู้ใช้
      - รองรับการเรียกใช้ API สำหรับการลบข้อมูลผู้ใช้ที่ส่งข้อมูลผ่าน HTTP DELETE

6.Totalattractions (components/pang/workshop5/attractions/totalattractions.js)

      - ฟังก์ชันและปุ่มที่เกี่ยวข้องกับ API ที่ใช้ดึงข้อมูลสถานที่ท่องเที่ยว
      - รองรับการเรียกใช้ API ต่าง ๆ เช่น fetchAttractionList, fetchAttractionSearch, fetchAttractionPagination, fetchAttractionSort, fetchAttrantionAll, fetchAtrantionLanguage, fetchAtrantionDetail, fetchAtrantionDetailLanguage, fetchAtrantionStatic

7.Petsale (components/pang/workshop5/petsale/petsale.js)

    - ฟังก์ชันและปุ่มที่เกี่ยวข้องกับ API ที่ใช้ดึงข้อมูลการขายสัตว์เลี้ยง
    - รองรับการเรียกใช้ API เช่น fetchPetSummary, fetchPetDaily

**การใช้งานและการเรียกใช้ API**

Totalapi
  - ปุ่ม USER LIST: เรียกดูรายชื่อผู้ใช้ทั้งหมด
  - ปุ่ม USER LIST (search): เรียกดูรายชื่อผู้ใช้ด้วยการค้นหา
  - ปุ่ม USER LIST (pagination): เรียกดูรายชื่อผู้ใช้แบบแบ่งหน้า
  - ปุ่ม USER LIST (sorted): เรียกดูรายชื่อผู้ใช้ที่ถูกเรียงลำดับ
  - ปุ่ม USER LIST (all): เรียกดูรายชื่อผู้ใช้ทั้งหมดพร้อมการค้นหาและเรียงลำดับ
  - ปุ่ม USER DETAIL: เรียกดูรายละเอียดของผู้ใช้

Create
  - ฟอร์มสร้างผู้ใช้: กรอกข้อมูลและกดปุ่ม Create User เพื่อสร้างผู้ใช้

Update
  - ฟอร์มอัปเดตข้อมูลผู้ใช้: กรอก ID และ Last Name และกดปุ่ม Update User เพื่อทำการอัปเดตข้อมูล

Delete
  - ฟอร์มลบผู้ใช้: กรอก ID และกดปุ่ม Delete Userrrr เพื่อทำการลบผู้ใช้

Totalattractions
  - ปุ่ม Attraction List: เรียกดูรายชื่อสถานที่ท่องเที่ยว
  - ปุ่ม Attraction Search: เรียกดูสถานที่ท่องเที่ยวด้วยคำค้นหา
  - ปุ่ม Attraction Pagination: เรียกดูรายชื่อสถานที่ท่องเที่ยวแบบแบ่งหน้า
  - ปุ่ม Attraction Sort: เรียกดูรายชื่อสถานที่ท่องเที่ยวที่ถูกเรียงลำดับ
  - ปุ่ม Attraction All: เรียกดูรายชื่อสถานที่ท่องเที่ยวทั้งหมดพร้อมคำค้นหาและการเรียงลำดับ
  - ปุ่ม Attraction Language: เรียกดูรายชื่อสถานที่ท่องเที่ยวในภาษาที่กำหนด
  - ปุ่ม Attraction Detail: เรียกดูรายละเอียดของสถานที่ท่องเที่ยว
  - ปุ่ม Attraction DetailLanguage: เรียกดูรายละเอียดของสถานที่ท่องเที่ยวในภาษาที่กำหนด
  - ปุ่ม Attraction Static: เรียกดูข้อมูลสถานที่ท่องเที่ยวทางสถิต

Petsale
  - ปุ่ม PetSales 7days: เรียกดูข้อมูลการขายสัตว์เลี้ยงในช่วง 7 วัน
  - ปุ่ม Pet Daily: เรียกดูข้อมูลการขายสัตว์เลี้ยงรายวัน