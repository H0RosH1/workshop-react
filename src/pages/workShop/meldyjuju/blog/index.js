import React from 'react'
import Navbarui2 from 'src/components/pang/navbarui2';
import Footer2 from 'src/components/pang/footer3';
import Footer3 from 'src/components/pang/footer2';
import Footer4 from 'src/components/pang/footer4';
import Bodyblog from 'src/components/pang/bodyblog';
import Blog from 'src/components/pang/blog';
import { Box } from "@mui/material";

const blog = () => {
  return (
    <div>
      <Navbarui2 />
      <Bodyblog />
      <Blog />
      <Box
        sx={{
          position: "relative",
          width: "100%",
          height: "200px",
        }}
      >
        <Box
          sx={{
            marginX: "5rem",
            position: "absolute",
            top: "0",
            left: "0",
            bottom: "0",
            right: "0",
            transform: "translate(0,-10em)",
            zIndex: 3, 
          }}
        >
          <Footer2 />
        </Box>
        <Box
          sx={{
            marginX: "5rem",
            marginBottom: "5rem",
            position: "absolute",
            top: "0",
            left: "0",
            bottom: "0",
            right: "0",
            transform: "translate(0,-5em)",
            zIndex: 2, 
          }}
        >
          <Footer3 />
        </Box>
        <Box
        sx={{ 
          
        zIndex: 1, 
          
        }}><Footer4 /></Box>
        

      </Box>
      {/* <Footer /> */}
    </div>
  );
};

export default blog;
