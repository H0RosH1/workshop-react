import React from "react";
import Navbar from "src/components/miw/Navbar";
import Footer from "src/components/miw/Footer";
import Calculator from "src/components/miw/Calculator";
import { Layout } from "src/layouts/dashboard/layout";

const cal = () => {
  return (
    <>
      <Navbar />
      <div className="p-5 text-center text-lg text-black">
        {" "}
        Calculator
        <Calculator />
      </div>
      <Footer />
    </>
  );
};
cal.getLayout = (cal) => <Layout>{cal}</Layout>;

export default cal;
