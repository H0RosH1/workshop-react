# Waiwitz Workshop

Workshop สำหรับ training การเขียนโปรแกรมด้วย framework Next.js
โดยแบ่งออกเป็น 6 Workshop

# Table Content

- [Getting Staterd (เริ่มต้น)](#getting-started)
  - [Prerequisites (ข้อกำหนด)](#prerequisites)
  - [Installation (การติดตั้ง)](#installation)
    - [Fork and Clone project](#fork-and-clone-project)
    - [Git Merge](#git-merge)
    - [Init project](#init-project)
- [Folder Structure](#folder-structure)
- [Development](#development)
  - [Workshop page](#workshop-page)
  - [Resume](#resume)
  - [Workshop 1 Calculator](#workshop-1-calculator)
  - [Workshop 2 UI](#workshop-2-ui)
  - [Workshop 3 Login API](#workshop-3-login-api)
  - [Workshop 4 get user by token login](#workshop-4-user-token)
  - [Workshop 5 API CRUD](#workshop-5-api-crud)
  - [Workshop 6 Basic E-commerce](#workshop-6-basic-e-commerce)

----
# Getting Staterd (เริ่มต้น)

## Prerequisites (ข้อกำหนด)

ให้ทำแต่ละ Workshop โดยสร้างหน้าโปรไฟล์เก็บ Workshop ของตัวเองตามในโฟลเดอร์ชื่อของแต่ละคน

## Installation

### Fork and Clone project

ก่อนที่จะทำการ Fork และ Clone project นำมาใช้จะต้องตั้งค่า git ผ่าน terminal

    git config --glbbal user.name "Name"
    git config --glbbal user.email "Email"
    git config --global credential.helper cache

หลังจากนั้นให้ fork project มาไว้ที่ Git ของตัวเองและ clone project ที่ได้ fork มา

    git clone https://gitlab.com/Waiwitz/workshop-react.git


### Git Merge
การ connect remote

    git remote add upstream <github-remote-repo-url> 
    git remote -v

การสร้าง branch ใหม่ก่อน commit

    git branch <Branch-Name>
    git checkout <Branch-Name>

การ push

    git add .
    git commit -m "Branch-Name"
    git push origin <Branch-Name>

การ pull 

    git checkout main
    git pull upstream main


### Init project

เมื่อ clone project มาแล้วให้เข้า terminal และเข้าถึงโฟล์เดอร์ของโปรเจคนั้น หรือใช้ terminal ของ vsCode เพื่อสร้าง package สำหรับการใช้งาน

    cd workshop-react
    npm i
    npm run dev

----
## Folder Structure

ใน Project จะมี Folder workShop สำหรับแต่ละคนให้สร้างหน้าเก็บ workshop ของตัวเองใน pages สำหรับ render หน้าและเก็บ components ของแต่ละหน้าแต่ละบุคคล

    my-app/
    ├─ node_modules/
    ├─ public/
    │  ├─ favicon.ico
    │  ├─ index.html
    │  ├─ robots.txt
    ├─ src/
    │  ├─ components/
    │  │  ├─ student1/
    │  │  ├─ student2/
    │  │  ├─ Waiwitz/
    │  ├─ contexts/
    │  ├─ guards/
    │  ├─ hocs/
    │  ├─ hooks/
    │  ├─ layouts/
    │  ├─ pages/
    │  │  ├─ auth/
    │  │  ├─ workShop/
    │  │  │  ├─ student1/
    │  │  │  ├─ student2/
    │  │  │  ├─ waiwitz/
    │  │  │  │  ├─ remuse/
    │  │  │  │  ├─ workshop1/
    │  │  │  │  ├─ workshop2/
    │  │  │  │  ├─ workshopAPI/   <== || workshop 3 & 4
    │  │  │  │  │  ├─ workshopLogin.js/
    │  │  │  │  │  ├─ workshopUser.js/
    │  │  │  │  ├─ workshopFive/
    │  │  │  │  ├─ workshopSix/
    │  ├─ sections/
    │  ├─ styles/
    │  ├─ theme/
    │  ├─ utils/
    ├─ .env.local
    ├─ .gitignore
    ├─ package.json
    ├─ README.md

# Development

## Workshop page

#### Components

Component ProfileBox จะแสดงเกี่ยวกับข้อมูลส่วนตัว

import components mui ที่ใช้สร้าง

    <!-- component mui -->
    import { Box, Card, CardContent, Typography, Grid, Paper, Container } from "@mui/material"

    <!-- Icon -->
    import EmailIcon from '@mui/icons-material/Email'; => icon Email
    import FacebookIcon from '@mui/icons-material/Facebook'; => icon Facebook


    Box => ใช้สำหรับทำเป็นกล่องครอบ Element อื่นๆ มีหน้าที่คล้ายๆ Div แต่สามารถตกแต่ง css ได้ด้วย attribute
    Card => การ์ดสำเร็จรูป
    CardContent => ส่วนเนื้อหาของด้านในการ์ดแบบ auto padding
    Typography => ตัวหนังสือที่สามารถกำหนดรูปแบบได้ผ่าน attribute
    Grid => ช่องแบ่งองค์ประกอบเป็นแถว
    Paper => คล้ายๆกับ Box แต่มีการใช้เงาที่มากกว่า

---

                        <Grid container spacing={3} columns={{ xs: 4, sm: 8.5, md: 11.5 }} justifyContent="center" className={styles.spaceContainer}>
                        <!-- กล่องรูปภาพฝั่งซ้าย -->
                            <Grid item xs={3}>
                                <Card style={{ height: '320px' }}>
                                    <CardContent>
                                        <Paper className={styles.profilePic}>
                                            <img src={profilePic} alt="profilepic" />
                                        </Paper>
                                    </CardContent>
                                </Card>
                            </Grid>

                              <!-- กล่องประวัติฝั่งขวา -->
                            <Grid item xs={8}>
                                <CardContent>
                                    <div>
                                        <Typography variant="h3" style={{ marginBottom: '1em' }}>Hi! I'm Waiwit Laoniyomthai 👀</Typography>
                                        <Typography margin='normal'>
                                            text
                                        </Typography>
                                        <div className={styles.contact}>
                                            <EmailIcon /> <div>waiwit.laoniyomthai@gmail.com</div>
                                        </div>
                                        <div className={styles.contact}>
                                            <FacebookIcon /> <div>Waiwit Laoniyomthai</div>
                                        </div>
                                    </div>
                                </CardContent>
                            </Grid>
                        </Grid>

---

Component MyWorkshop จะเป็นส่วนแสดงรายการ workshop ทั้งหมดและ Link สำหรับเข้าไปที่ workshop นั้น

import components mui และ external css style

    import styles from "src/styles/waiwitzStyles.module.css";
    import { Box, Button, Card, CardHeader, CardContent, Container, Typography, Paper, Grid, Link } from "@mui/material";

    CardHeader => ส่วนหัวของการ์ด
    Container => กรอบ Element
    Link => มีหน้าที่คล้าย tag a ใช้สำหรับเชื่อมไปหน้า page อื่น

---

Array รวม workshop

    const workshop = [
        {
            name: 'Workshop Resume 🧾',
            link: 'waiwitz/resume/resumeAboutMe'
        }, ...// The rest of array
    ]

---

                    <Card>
                        <CardHeader title='Workshop 💪' />
                        <CardContent>
                            <Grid container spacing={3}>
                            <!-- Loop workshop -->
                                {workshop.map(((work) => (
                                    <Grid item key={work.name} >
                                        <Link href={work.link}>
                                            <Button style={{ backgroundColor: '#eee', color: '#001' }}>
                                                {work.name}
                                            </Button>
                                        </Link>
                                    </Grid>
                                )))}
                            </Grid>
                        </CardContent>
                    </Card>

---

#### การใช้ Components

import component และใช้บนหน้าที่ต้องการได้ปกติ

    import ProfileBox from "src/components/waiwitz/profileBox";
    import MyWorkshop from "src/components/waiwitz/myworkshop";

    const waiwitz = () => {
        return (
            <>
                <ProfileBox />  --> เรียกใช้ component
                <MyWorkshop/>
                <div style={{marginBottom: '5em'}}></div>
            </>
        )
    }

    waiwitz.getLayout = (waiwitz) => <Layout>{waiwitz}</Layout>;
    export default waiwitz;

## Resume

#### Components

Component NavBar แสดง navbar

import component mui และ external css เพื่อนำมาสร้าง navbar

    import { MenuItem, Typography, Toolbar, AppBar} from '@mui/material';
    import Link from 'next/link';
    import styles from "src/styles/waiwitzStyles.module.css";

    MenuItem => ใช้สำหรับเป็นปุ่มกด menu
    AppBar => แถบ Navbar
    Toolbar => ใช้สำหรับเป็นเนื้อหาใน Navbar

---

        export default function NavBar() {
    return (
        <AppBar position="static" className={styles.navBar}>
            <Toolbar>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                <b className={styles.logo}>My Resume</b>
            </Typography>
            <Link href="resumeAboutMe" className={styles.navMenuLink}>
                <MenuItem>
                About ME!
                </MenuItem>
            </Link>
            <Link href="resumeProject" className={styles.navMenuLink}>
                <MenuItem>
                Project
                </MenuItem>
            </Link>
            </Toolbar>
        </AppBar>
    );

}

#### resumeAboutMe.js

import component mui และ external css เพื่อนำมาตกแต่งและสร้าง resume

    import Head from 'next/head'
    import styles from "src/styles/waiwitzStyles.module.css";
    import NavBar from 'src/components/waiwitz/navBar'
    import { Container, Grid } from '@mui/material';
    import EmailIcon from '@mui/icons-material/Email';
    import FacebookIcon from '@mui/icons-material/Facebook';

---

การเรียกใช้ components

    export default function Home() {
    return (
    <>
            <NavBar /> ==> เรียกใช้ component NavBar
            <Container>
            ... // another fixed code
            </Container>
    </>

    )
    }

#### resumeProject.js

import component mui และ external css เพื่อนำมาตกแต่งและสร้าง resume

    import Head from 'next/head'
    import styles from "src/styles/waiwitzStyles.module.css";
    import NavBar from 'src/components/waiwitz/navBar'
    import { Container, Grid } from '@mui/material';

---

Array เก็บ Tag ของ Project

    let bookstoreTag = ['Full Stack', 'React Native', 'Firebase', 'Expo'];
    ... // another tag

---

            <NavBar /> ==> เรียกใช้ component NavBar
                <Grid container spacing={1} className={styles.projectInfo} columns={{ xs: 4, sm: 8, md: 12 }} justifyContent="center">
                        <!-- รายละเอียด -->
                        <Grid item xs={8}>
                            <div>
                                <h2>Mini Project Book store application</h2>
                                <div className={styles.tagAlign}>

                                    {/* loop bookstoreTag */}
                                    {bookstoreTag.map((item) => (
                                        <div className={styles.tag}>
                                            {item}
                                        </div>
                                    ))}

                                </div>
                                <div>
                                 // คำอธิบาย
                                </div>
                            </div>
                        </Grid>
                    </Grid>
                    ...... // another project

## Workshop 1 Calculator

#### calculator.js

import components mui กับ external css

    import Head from "next/head";
    import { Layout } from "src/layouts/dashboard/layout";
    import styles from "src/styles/waiwitzStyles.module.css";
    import { Typography, Button, Link, Breadcrumbs } from "@mui/material";
    import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
    import { useState } from "react";

    Head => คล้ายกับ tag head html
    Layout => ดึงนำเอา layout ของ tamplate มาใช้ในหน้า
    Button => ปุ่มกดสามารถกำหนดรูปแบบได้ผ่าน attribute
    Breadcrumbs => ลิสต์ของลิงก์บ่งบอกตำแหน่งของหน้าที่อยู่
    ArrowBackIosIcon => ไอคอนรูปศร
    useState => ใช้สำหรับ set ค่า เป็น hook ของ react

---

ตัวแปรที่นำไปใช้ในการแสดงผล

    const [input, setInput] = useState('');

    const operators = ['/', '*', '-', '+'];
    const numbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", '.'];
    const errMsg = 'โปรดใส่เลขให้ถูกต้อง'
    const undefindMsg = 'ไม่พบค่า'

---

function handleClick ใช้เพื่อรับค่าตัวเลขและเครื่องหมาย เมื่อกดที่ปุ่มจะ set ค่าตัวเลขไว้ใน state ตัวแปร input

    const handleClick = (value) => {
            try {
                const lastchar = input.slice(-1)
                if (operators.includes(lastchar) && operators.includes(value.target.textContent)) {
                    setInput(input.slice(0, -1).concat(value.target.textContent));
                } else if (input === errMsg || input === undefindMsg) {
                    setInput(input.replace(undefindMsg,value.target.textContent));
                } else {
                    setInput(input.concat(value.target.textContent));
                }
            } catch (error) {
                setInput(errMsg)
            }
            console.log(input);
        }

        <div className={styles.keyPad}>
            {numbers.map((number) => (
            <Button key={number} onClick={handleClick}>{number}</Button>
            )}
        </div>

---

function clear ใช้เพื่อเคลียร์ค่าใน input เมื่อกดที่ปุ่ม c

    const clear = () => {
        setInput('');
    }

    <Button onClick={clear}>c</Button>

---

function startCalculate ใช้เพื่อนำค่าใน input มาคำนวณและแสดงผลเมื่อกดที่ปุ่ม =

    const startCalculate = () => {
            try {
                setInput(eval(input).toString());
            } catch (error) {
                setInput('');
            }
        }

    <Button onClick={startCalculate}>=</Button>

---

function percent ใช้เพื่อคำนวณหาร้อยละเพื่อกดที่ปุ่ม %

    const percent = () => {
        try {
            if (!eval(input / 100)) {
                setInput(undefindMsg);
            } else {
                setInput(eval(input / 100).toString());
            }

            console.log(input);
            // setInput(eval(input / 100).toString());
            // if (input == undefined || input == NaN || input == null) setInput('ไม่พบค่า');
        } catch (error) {
            setInput('');
        }
    }

    <Button onClick={percent}>%</Button>

---

function invertInt ใช้เพื่อแปรค่าเป็นจำนวนเต็มบวกหรือลบเมื่อกดที่ปุ่ม +/-

    const invertInt = () => {
            // console.log('ตัวแรก ' + input.slice(0, 1));
            if (input.slice(0, 1) == '-') {
                setInput(input.substring(1));
            } else {
                setInput('-' + input);
            }
        }

    <Button onClick={invertInt}>+/-</Button>

## Workshop 2 UI

#### index.js

#### Components

    <BannerHome />  ==> Banner ของหน้า Home
    <Navbar />  ==> navbar สำหรับใส่ทุกหน้า
    <IntroBox />  ==> กล่องแสดงผลสำหรับหน้าแรก
    <Footer />  ==> footer แสดงผลทุกหน้า
    <Introduce1 />  --------
    <Introduce2 />          |
    <Introduce3 />          | ==>  กล่องแสดงผลสำหรับ reuse ใช้แสดงผลทุกหน้า
    <Introduce4 />  --------
    <BackButton />   ==> ปุ่มกดกลับไปหน้ารวม workShop
    <ListText /> ==> ลิสต์รายการข้อมูลใช้ใน component introduce ต่างๆ
    <Team> ==> กล่องแสดงชื่อและตำแหน่ง

---

Array introboxData ใช้สำหรับนำไปแสดงผลคู่กับ component Introbox

    const introboxData = [
        {
            title: 'HOME DESIGN CONSULTATION',
            detail: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.',
        },
        {
            title: 'HOME DESIGN 3D 2D INTERIOR SERVICE',
            detail: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.',
        },
        {
            title: 'HOME DESIGN CONSULTATION',
            detail: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.',
        }
    ];

---

function findOdd หาค่าเลขคี่ index เพื่อนำไปใช้ในการแสดงผลสีตามเลขคู่หรือเลขคี่ สามารถใช้ได้กับ component introBox

    const findOdd = (index) => {
        let cal = index % 2
        return cal;
    }

    {introboxData.map((item, index) => (
        <IntroBox title={item.title} detail={item.detail} color={findOdd(index) !== 0 ? '#787474' : '#201414'} />
    ))}

เรียกใช้ component เพื่อแสดงผลข้อมูลและเติมข้อมูลที่ต้องการผ่าน parameter

    <Container maxWidth='xl'>
        <Introduce2 head1={'PERFECT PARTNER'} head2={'WE HAVE PRIORITY FOR CAN CREATE DREAM HOME DESIGN'} body={`Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.`} />
        <Introduce3 head1={'TRUST US NOW'} head2={'WHY CHOICE OUR HOME DESIGN INTERIOR SERVICES'} body={`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.`} />
        <Introduce4 head1={'CLIENTS FEEDBACK'} head2={'OUR TESTIMONIAL FROM BEST CLIENTS'} body={`Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.`} />
    </Container>

#### aboutUs.js

Array ใช้สำหรับนำไปแสดงผลโดย loop คู่กับ component team

    const teamList = [
        {
            name: 'John',
            position: 'Founder'
        },
        {
            name: 'Alice',
            position: 'Founder'
        },
        {
            name: 'James',
            position: 'Founder'
        },
        {
            name: 'Betty',
            position: 'Founder'
        },
        {
            name: 'August',
            position: 'Founder'
        },
        {
            name: 'Inez',
            position: 'Founder'
        },
    ]

    <Grid container columns={{ xs: 4, sm: 8, md: 12 }} spacing={4}>
        {teamList.map((team, index) => (
            <Grid item xs={4} key={team.name}>
                <Team name={team.name} position={team.position} index={index} />
            </Grid>
        ))}
    </Grid>

เพราะ Card ใน component Team ต้องแสดงผลแบบคละสีจึงต้องมีการรับ index เข้าไปเพื่อเช็คหาเลขคี่

    let findOdd = index % 2;
    <CardContent sx={{ textAlign: 'center', bgcolor: findOdd !== 0 ? '#fff' : '#1B1717' }}>
                ...//
    </CardContent>

## Workshop 3 Login API

#### workshopLogin.js

ตัวแปรที่ใช้เก็บค่าและเช็ค valid

    const [username, setUsername] = useState('karn.yong@melivecode.com');
    const [password, setPassword] = useState('melivecode');
    const [validUsername, setValidUsername] = useState(false);
    const [validPassword, setvalidPassword] = useState(false);
    const [helperTextUsername, setHelperTextUsername] = useState();
    const [helperTextPassword, setHelperTextPassword] = useState();

function Login ใช้เพื่อ fetch หา user

    const Login = async (user) => {
    try {
    const data = await fetch(`${process.env.NEXT_PUBLIC_API_LOGIN_URL}`, {
    method: 'POST',
    headers: {
    'Content-Type': 'application/json'
    },
    body: JSON.stringify(user)
    });
    return await data.json();
    } catch (error) {
    console.log(error)
    }
    }

function handleSubmit ใช้เมื่อกด submit form โดยจะ requset หา user ด้วย function Login

    const handleSubmit = async (event) => {
            event.preventDefault();
            const res = await Login({
                username, password
            });
            try {
                <!-- เช็คว่าช่องกรอกว่างหรือไม่ -->
                username ? setValidUsername(false) & setHelperTextUsername('') : ''
                password ? setvalidPassword(false) & setHelperTextPassword('') : ''
                console.log(res);
                if ('accessToken' in res) {
                    localStorage.setItem('Token', res['accessToken']);
                    console.info('เข้าสู่ระบบสำเร็จ');
                    console.log(`Token : ${localStorage.getItem('Token')}`)
                    alert(`เข้าสู่ระบบสำเร็จ`)
                    window.location.href = '/workShop/waiwitz/workshopAPI/workshopUser'
                } else {
                    console.error(`status : ${res.status}`);
                    console.info(`info : ${res.message}`);

                <!-- เช็คว่าช่องกรอกว่างหรือไม่ -->
                    !username ? setValidUsername(true) & setHelperTextUsername('กรุณากรอกชื่อผู้ใช้') : ''
                    !password ? setvalidPassword(true) & setHelperTextPassword('กรุณากรอกรหัสผ่าน') : ''
                    <!--  ถ้าเป็นช่องว่างให้ log ข้อความผิดพลาด -->
                    if (!username || !password) {
                        console.info(`ช่องชื่อผู้ใช้หรือรหัสผ่านว่าง `);
                        console.info(`username : '${username}', password : '${password}'`);

                        <!--  ถ้าชื่อผู้ใช้หรือรหัสผ่านไม่ตรงกับที่ req -->
                    } else if (username != res.username || password != res.password) {
                        console.info(res.message)
                        alert('ชื่อผู้ใช้หรือรหัสผ่านไม่ตรง')
                        // console.info(`username : ${username == res.username ? 'ตรง' : 'ไม่ตรง'}, password : ${res.password == password ? 'ตรง' : 'ไม่ตรง'}`)
                    }

                }
            } catch (error) {
                console.error(error);
                alert(error)
            }
        }

หากมีการเก็บ Token ที่ยังไม่หมดอายุไว้ใน localStorage อยู่แล้ว เมื่อเข้ามาที่หน้า Login จะถูกย้ายไปที่หน้า Workshop 4 ที่แสดงข้อมูล user

    const token = localStorage.getItem('Token');
        if (token) {
            window.location.href = '/workShop/waiwitz/workshopAPI/workshopUser'
        }

## Workshop 4 get user by token login

#### workshopUser.js

    function Logout ใช้กับปุ่มกดเพื่อออกจากระบบและลบข้อมูลใน localStorage

    const Logout = () => {
        localStorage.removeItem('Token');
        localStorage.removeItem('User');
        window.location.href = '/workShop/waiwitz/workshopAPI/workshopLogin';
    }
           <Button color="inherit" onClick={Logout} sx={{ bgcolor: '#a70000', borderRadius: '0' }}>
                                        <LogoutIcon />
                                        Logout</Button>

ในส่วนของการเช็ค User จะมีการใช้ useEffect Hook เพื่อให้ fetch ข้อมูลออกมาตลอดและเช็คข้อมูลว่า token หมดอายุหรือยัง

    const jwt = localStorage.getItem('Token'); // รับ Token
        const [userData, setUserData] = useState(null)
        try {
            useEffect(() => {
                fetch(`${process.env.NEXT_PUBLIC_API_USER_URL}`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${jwt}`,
                        'Content-Type': 'application/json'
                    },
                }).then(async (res) => {
                    if (res.ok) {  // หาก fetch ข้อมูลออกมาสำเร็จ
                        const data = await res.json()
                        setUserData(true); // ให้เช็คว่าเข้าสู่ระบบแล้ว
                        localStorage.setItem('User', JSON.stringify(data.user)); // เพิ่มข้อมูลลง localStorage
                        console.info('request :', res);
                        console.info('เข้าสู่ระบบแล้ว');
                        console.log(`ข้อมูลผู้ใช้ : `, await JSON.parse(localStorage.getItem('User')));
                    } else { // หาก fetch ข้อมูลออกมาไม่ได้
                        localStorage.removeItem('Token'); // ให้ลบข้อมูลใน localStorage ในกรณีที่ token หมดอายุ
                        localStorage.removeItem('User');
                        console.error('สถานะ: ', res.statusText)
                        if (!userData) console.info('ยังไม่ได้เข้าสู่ระบบหรือ token timeout')
                        // alert('ยังไม่ได้เข้าสู่ระบบ')
                    }
                });
            }, [0])

หากเข้าสู่ระบบสำเร็จจะเรียกข้อมูลใน localStorage มาไว้ในตัวแปรและนำไปใช้แสดงผล

        const user = JSON.parse(localStorage.getItem('User'));
         <Grid item>
            <b>Username :</b> {user.fname} <br />
            <b>Name :</b> {user.fname} {user.lname} <br />
            <b>email :</b> {user.email}
        </Grid>

## Workshop 5 API CRUD

### Components

#### userForm.js

component จะมีการรับค่า parameter จาก state เพื่อใช้เซ็ทค่า value และ onChange นำไปใช้เมื่อ submit

    const UserForm = ({ form, username, fname, lname, password, email, getApi, setUsername, setFname, setLname, setPassword, setEmail }) => {
        return (
            <Box textAlign={'center'} padding={'0 8em'}>
                {username}
                <TextField label="Username" sx={{ m: 1, width: '100%' }} defaultValue={username} onChange={e => setUsername(e.target.value)} />
                <TextField label="fname" sx={{ m: 1, width: '100%' }} defaultValue={fname} onChange={e => setFname(e.target.value)} />
                <TextField label="lname" sx={{ m: 1, width: '100%' }} defaultValue={lname} onChange={e => setLname(e.target.value)} />

                <!-- เช็คว่าฟอร์มที่รับค่ามาเป็นฟอร์มเพิ่ม user หรือไม่ -->
                {form == 'createUser' ?
                    <>
                        <TextField label="password" sx={{ m: 1, width: '100%' }} defaultValue={password} onChange={e => setPassword(e.target.value)} />
                        <TextField label="email" sx={{ m: 1, width: '100%' }} defaultValue={email} onChange={e => setEmail(e.target.value)} />
                    </>
                    :
                    ''
                }
                <Button variant="outlined" sx={{ width: '100%' }} onClick={getApi}>
                    Save
                </Button>
            </Box>
        )
    }

#### userList.js

component นี้จะมีการรับค่า parameter จาก state เพื่อนำมาแสดงผล

users => array users \
openEdit => function open modal edit user \
openEdit => function open modal delete user \
setUsers => set array for users state

    const UserList = ({ users, openEdit, openDelete, setUsers }) => {

        useEffect(() => {
            const fetchAllUser = () => {
                fetch(`${process.env.NEXT_PUBLIC_USERLIST_APIUSERS}`, {
                    method: `GET`,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                }).then(async (res) => {
                    try {
                        const data = await res.json();
                        if (res.ok) {
                            if (data.message) console.info(data.message);
                            setUsers(data)
                        } else {
                            if (data.message) console.info(data.message);
                            console.error('Status : ', res.statusText);
                        }
                    } catch (error) {
                        console.error('Error : ', error);
                    }
                });
            }
            const intervalId = setInterval(fetchAllUser, 2000);
            return () => clearInterval(intervalId);
        }, [])

        return (
            // ยกมาเฉพาะส่วนแถวตาราง
            ....// loop users array
              {users.map((user, index) => (
                                    <TableRow
                                        key={index}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell component="th" scope="row">
                                            <img src={user.avatar} width={50} height={50} />
                                        </TableCell>
                                        <TableCell component="th" scope="row">
                                            {user.username}
                                        </TableCell>
                                        <TableCell component="th" scope="row">
                                            {user.fname} {user.lname}
                                        </TableCell>
                                        <TableCell component="th" scope="row">
                                            <Button onClick={() => openEdit(user)}>
                                                Edit
                                            </Button>
                                            <Button onClick={() => openDelete(user)} >
                                                Delete
                                            </Button>
                                        </TableCell>
                                    </TableRow>
                                ))}
        )
    }

#### การใช้งาน component

#### index.js

ให้ import component เพื่อนำมาใช้งาน

    import UserList from "src/components/waiwitz/workshop5/userList";
    import UserForm from "src/components/waiwitz/workshop5/userForm";

จะมีใช้ state เพื่อเก็บข้อมูลผู้ใช้ function และ input ต่างๆ

    const [users, setUsers] = useState([])
    const [open, setOpen] = useState(false);
    const [openEdit, setOpenEdit] = useState(false)
    const [openDelete, setOpenDelete] = useState(false)
    const [api, setApi] = useState(null);
    const [form, setForm] = useState('');

    const [userId, setUserId] = useState();
    const [fname, setFname] = useState();
    const [lname, setLname] = useState();
    const [username, setUsername] = useState();
    const [password, setPassword] = useState();
    const [email, setEmail] = useState();
    const [avatar, setAvatar] = useState('https://www.melivecode.com/users/cat.png');

function handleOpen ใช้สำหรับกดปุ่ม

    const handleOpen = (i) => {
            let config = i.config;
            let crud = i.crud;
            setOpenEdit(false);
            setOpen(true);

            if (config) setApi(config);
            if (crud) setForm(crud)

            setUserId('')
            setUsername('');
            setPassword('');
            setFname('');
            setLname('');
            setEmail('');
            // if (crud == 'update_delete_user') {
            //     CallUserlist();
            // }
        }

function handleOpenEdit และ handleOpenDelete ใช้สำหรับกดเปิด modal ภายใน component userList โดยใส่ฟังก์ชั่นนี้ลงไปใน parameter ของปุ่ม เมื่อกดแล้วจะ set ค่าที่ได้รับมาจากปุ่มที่ loop ตามข้อมูลมา

    const handleOpenEdit = (user) => {
            setOpen(false);
            setOpenEdit(true);

            setUserId(user.id)
            setUsername(user.username);
            setPassword(user.password);
            setFname(user.fname);
            setLname(user.lname);
            setEmail(user.email);
        }


    const handleOpenDelete = (user) => {
        setOpen(false);
        setOpenEdit(false);
        setOpenDelete(true);
        setUserId(user.id)
        setUsername(user.username);
    }

    // การใช้งาน
     <UserList users={users} openEdit={handleOpenEdit} openDelete={handleOpenDelete} 


function handleClose ใช้สำหรับปิดหน้า modal

    const handleClose = () => {
        setOpen(false);
        setOpenEdit(false);
        setOpenDelete(false);
    }

Object และ Array ใช้สำหรับนำข้อมูลส่งไปให้ API

     const userField = {
        "id": userId,
        "fname": fname,
        "lname": lname,
        "username": username,
        "password": password,
        "email": email,
        "avatar": avatar
    };


    const userListApi = [
        {
            apiName: 'USER LIST: api/users',
            callApi: () => callGetApi(process.env.NEXT_PUBLIC_USERLIST_APIUSERS, 'USER LIST', 'GET'),
        },
        {
            apiName: 'USER CREATE',
            config: {
                url: process.env.NEXT_PUBLIC_USERLIST_CREATE,
                apiName: 'USER CREATE: api/users/create',
                method: 'POST',
            },
            crud: 'createUser'
        },
        {
            apiName: 'USER UPDATE AND DELETE',
            crud: 'update_delete_user'
        },
    ];


----

Function callGetApi ใช้สำหรับการ requset ข้อมูลเมื่อกดปุ่ม โดยรับ API และค่าต่างๆด้วย parameter จากปุ่มที่กด
 
    const callGetApi = async (api, apiName, apiMethod, reqBody) => {
            fetch(`${api}`, {
                method: `${apiMethod}`,
                headers: {
                    'Content-Type': 'application/json'
                },
                ...(apiMethod == 'POST' || apiMethod == 'PUT' || apiMethod == 'DELETE' ? { body: JSON.stringify(reqBody) } : {}) // เงื่อนไขเช็ค method
            }).then(async (res) => {
                try {
                    const data = await res.json();
                    console.log(res)
                    if (res.ok) {
                        if (data.message) {
                            console.info(`เรียก api สำเร็จ แต่ ${data.message}`);
                            alert(data.message);
                        } else {
                            console.info(`เรียก api ${apiName} สำเร็จ ✅`);
                            alert(`เรียก api ${apiName} สำเร็จ`);
                        }
                        console.log('รายละเอียด :', data);
                        return data;
                    } else {
                        alert(`เรียก api ${apiName} ไม่สำเร็จ ${data.message}`);
                        console.error(`เรียก api ${apiName} ไม่สำเร็จ ❌`)
                        if (data.message) console.info(data.message);
                        console.error('Status : ', res.statusText);
                    }
                } catch (error) {
                    alert('มีข้อผิดพลาดเกิดขึ้น ! 🥲');
                    console.error('Error : ', error);
                    console.info(res)
                }
            });
        }

ปุ่มกดจะสร้างจาก array UserListApi โดย loop แสดงผลปุ่มออกมาและเช็คเงื่อนไขว่าภายใน object นั้นมีข้อมูลชื่อ crud หรือไม่
หากมีจะการกดเป็นการให้เปิด modal

    <Box>
        {userListApi.map((item, index) => (
            <Button key={index} onClick={!item.crud ? item.callApi : () => handleOpen(item)}
                                                style={{
                                                    backgroundColor: '#eee',
                                                    color: '#001',
                                                    width: '100%',
                                                    margin: '5px 0'
                                                }}>
                                                {item.apiName}
                                                {item.crud ? <KeyboardArrowRightRoundedIcon
                                                    sx={{ bgcolor: '#d4d4d4', marginLeft: '10px', borderRadius: '100%' }} /> : ''}
            </Button>
                                        ))}
    </Box>

อีกทั้งภายใน Modal จะแสดงผลตามเงื่อนไขจากที่ได้รับ parameter และนำไป set ที่ state form ว่าเป็นรูปแบบใด ให้แสดงตาม form

        <Modal
                            {form == 'createUser' ? // เงื่อนไข
                                <>
                                    <Typography gutterBottom variant="h4">
                                        Create User
                                    </Typography>
                                    <UserForm
                                        form={form}
                                        setUsername={setUsername}
                                        setFname={setFname}
                                        setLname={setLname}
                                        setPassword={setPassword}
                                        setEmail={setEmail}
                                        username={username}
                                        fname={fname}
                                        lname={lname}
                                        password={password}
                                        email={email}
                                        getApi={() => { callGetApi(api.url, api.apiName, api.method, userField); handleClose(); }}
                                    />
                                </>
                                :
                                <></>
                            }

                            {form == 'update_delete_user' ? // เงื่อนไข
                                <UserList users={users} openEdit={handleOpenEdit} openDelete={handleOpenDelete} setUsers={setUsers} />
                                :
                                <></>
                            }
            </Modal>

----
Object แก้ไขและลบ จะแยกไว้สำหรับ Modal เฉพาะเพื่อนำไปใช้กับการ submit กับ function callApI โดยรับค่ามาจากการ set ค่าของ function handleOpenEdit และ handleOpenDelete

    const configUpdate = {
        url: process.env.NEXT_PUBLIC_USERLIST_UPDATE,
        apiName: 'USER UPDATE: api/users/update',
        method: 'PUT',
    }

    // การใช้งานบน modal แก้ไขโดยกับ component UserForm
    <UserForm setUsername={setUsername}
                                setFname={setFname}
                                setLname={setLname}
                                setPassword={setPassword}
                                setEmail={setEmail}
                                username={username}
                                fname={fname}
                                lname={lname}
                                password={password}
                                email={email}
                                getApi={() => { callGetApi(configUpdate.url, configUpdate.apiName, configUpdate.method, userField); handleClose(); }}
                            />

    const configDelete = {
        url: process.env.NEXT_PUBLIC_USERLIST_DELETE,
        apiName: 'USER DELETE: api/users/delete',
        method: 'DELETE',
    }
        // การใ้ช้งานบน modal ลบ
     <Button variant="contained" onClick={() => { callGetApi(configDelete.url, configDelete.apiName, configDelete.method, userField); handleClose() }}>Delete</Button>


## Workshop 6 Basic E-commerce

workshop 6 เกี่ยวกับการสร้างระบบซื้อของเบื้องต้น โดยจะมีระบบการค้นหาแบบกรอง นำสินค้าลงตระกร้า เข้าสู่ระบบ และ Middleware