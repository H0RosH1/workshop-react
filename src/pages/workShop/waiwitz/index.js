import { Layout } from "src/layouts/dashboard/layout";
import Repice from "src/components/waiwitz/recipe";
import { Container } from "@mui/material"
import MyWorkshop from "src/components/waiwitz/myworkshop";
import ProfileBox from "src/components/waiwitz/ProfileBox";
const waiwitz = () => {
    return (
        <>
            <ProfileBox />
            <MyWorkshop/>
            <Repice />
            <div style={{marginBottom: '5em'}}></div>
        </>
    )
} 

waiwitz.getLayout = (waiwitz) => <Layout>{waiwitz}</Layout>;
export default waiwitz; 