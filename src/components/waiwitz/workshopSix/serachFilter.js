import { Box, InputBase, styled, alpha, TextField, Accordion, AccordionSummary, Typography, AccordionDetails, List, ListItem, ListItemAvatar, ListItemText, FormControlLabel, Checkbox, Divider, FormControl, InputLabel, Select, MenuItem, OutlinedInput } from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { useState } from "react";

const SearchFilter = ({ inputSetState, categories, selectCate, cateSetState, selectPrice, priceSetState}) => {
    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                width: 250,
            },
        },
    };

    const handleChangePrice = (event) => {
        priceSetState(event.target.value);
      };
    
    const handleChangeCate = (event) => {
        const {
            target: { value },
        } = event;
        cateSetState(
            // On autofill we get a stringified value.
            typeof value === 'string' ? value.split(',') : value,
        );
    };


    return (
        <Box padding={2} mb={5} border={'solid 1px #ddd'} display={'flex'} justifyContent={'space-between'}>
            <TextField sx={{ m: 1 }} label="Search" variant="outlined" onChange={(e) => inputSetState(e.target.value)} />
            <Box>
                <FormControl variant="filled" sx={{ m: 1, minWidth: 120 }}>
                    <InputLabel id="demo-simple-select-filled-label">Price</InputLabel>
                    <Select
                        labelId="demo-simple-select-filled-label"
                        id="demo-simple-select-filled"
                        value={selectPrice}
                        onChange={handleChangePrice}
                    >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        <MenuItem value={'lowToHigh'}>Low to High</MenuItem>
                        <MenuItem value={'highToLow'}>High to Low</MenuItem>
                    </Select>
                </FormControl>
                <FormControl sx={{ m: 1, width: 200 }}>
                    <InputLabel id="demo-multiple-checkbox-label">Categories</InputLabel>
                    <Select
                        labelId="demo-multiple-checkbox-label"
                        id="demo-multiple-checkbox"
                        multiple
                        value={selectCate}
                        onChange={handleChangeCate}
                        input={<OutlinedInput label="Categories" />}
                        renderValue={(selected) => selected.join(', ')}
                        MenuProps={MenuProps}
                    >
                        {categories.map((cate) => (
                            <MenuItem key={cate} value={cate}>
                                <Checkbox checked={selectCate.indexOf(cate) > -1} />
                                <ListItemText primary={cate} />
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>


            </Box>
        </Box>
    )
}

export default SearchFilter;