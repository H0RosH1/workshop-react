const { Grid } = require("@mui/material")
import CheckCircleRoundedIcon from '@mui/icons-material/CheckCircleRounded';

const ListText = () => {
    return (
        <Grid container flexGrow={0} marginTop={3}>
            <Grid item sx={{ marginRight: '15%' }}>
                <ul>
                    <li><CheckCircleRoundedIcon sx={{ color: '#757575', marginRight: '10px' }} />Flexible Time</li>
                    <li><CheckCircleRoundedIcon sx={{ color: '#757575', marginRight: '10px' }} />Perfect Work</li>
                    <li><CheckCircleRoundedIcon sx={{ color: '#757575', marginRight: '10px' }} />Client Priority</li>
                </ul>
            </Grid>
            <Grid item>
                <ul>
                    <li><CheckCircleRoundedIcon sx={{ color: '#757575', marginRight: '10px' }} />Flexible Time</li>
                    <li><CheckCircleRoundedIcon sx={{ color: '#757575', marginRight: '10px' }} />Perfect Work</li>
                    <li><CheckCircleRoundedIcon sx={{ color: '#757575', marginRight: '10px' }} />Client Priority</li>
                </ul>
            </Grid>
        </Grid>
    )
}

export default ListText;