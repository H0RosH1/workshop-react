import React from 'react'
import { AppBar, Toolbar, Typography, Button } from '@mui/material';

import MenuProject from "./menuProject"
import MenuPage from "./menuPage"

export default function nav() {
    return (
        <div>
            <AppBar position="static" sx={{ padding: 0, margin: 0,borderBottom:'1px solid white' }} >
                <Toolbar sx={{ backgroundColor: "#19A7CE" }}>
                    
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1, margin: 0 }} className="tracking-[.17em] p-0 m-0">
                        HOMCO
                    </Typography>
                    <Button href='./' color="inherit" sx={{ "&:hover": { backgroundColor: "#46b8d7" } }} >Home</Button>
                    <Button color="inherit" sx={{ "&:hover": { backgroundColor: "#46b8d7" } }} >About</Button>
                    <MenuProject />
                    {/* <Button color="inherit" sx={{ "&:hover": { backgroundColor: "#4338ca" } }} >Our Services</Button> */}
                    <Button color="inherit" sx={{ "&:hover": { backgroundColor: "#46b8d7" } }} >portfolio</Button>
                    {/* <Button color="inherit" sx={{ "&:hover": { backgroundColor: "#4338ca" } }} >Page</Button> */}
                    <MenuPage/>
                </Toolbar>
            </AppBar>
        </div>
    )
}
