import React, { useState, useEffect } from 'react';
import { Container, Typography, TextField, Button, CircularProgress } from '@mui/material';
import CLayout from '../cLayout';

export default function UpdateUser() {
  const [formData, setFormData] = useState({
    fname: '',
    lname: '',
    username: '',
    password: '',
    email: '',
    avatar: '',
  });
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const response = await fetch('https://www.melivecode.com/api/users/12');
        const res = await response.json();
        const userData = await res.user
        console.log(userData)
        setFormData(userData);
        setLoading(false);
      } catch (error) {
        console.error('Error fetching user data:', error);
        setLoading(false);
      }
    };

    fetchUserData();
  }, []);

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    setLoading(true);

    try {
      const response = await fetch('https://www.melivecode.com/api/users/update', {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });

      if (!response.ok) {
        throw new Error('Failed to update user');
      }

      const updatedUserData = await response.json();
      console.log('User updated successfully:', updatedUserData);
    } catch (error) {
      console.error('Error updating user:', error);
    } finally {
      setLoading(false);
    }
  };

  return (
    <CLayout title='Update User'>
      <Container>
        <form onSubmit={handleSubmit}>
          <TextField
            label='First Name'
            name='fname'
            value={formData.fname}
            onChange={handleChange}
            fullWidth
            required
            sx={{ marginBottom: '10px' }}
          />
          <TextField
            label='Last Name'
            name='lname'
            value={formData.lname}
            onChange={handleChange}
            fullWidth
            required
            sx={{ marginBottom: '10px' }}
          />
          <TextField
            label='Username'
            name='username'
            value={formData.username}
            onChange={handleChange}
            fullWidth
            required
            sx={{ marginBottom: '10px' }}
          />
          <TextField
            label='Password'
            type='password'
            name='password'
            value={formData.password}
            onChange={handleChange}
            fullWidth
            required
            sx={{ marginBottom: '10px' }}
          />
          <TextField
            label='Email'
            type='email'
            name='email'
            value={formData.email}
            onChange={handleChange}
            fullWidth
            required
            sx={{ marginBottom: '10px' }}
          />
          <TextField
            label='Avatar URL'
            name='avatar'
            value={formData.avatar}
            onChange={handleChange}
            fullWidth
            sx={{ marginBottom: '10px' }}
          />

          <Button
            type='submit'
            disabled={loading}
            sx={{
              backgroundColor: 'black',
              color: 'white',
              borderRadius: '10px',
              '&:hover': {
                backgroundColor: '#373737',
              },
            }}
          >
            {loading ? <CircularProgress size={24} /> : 'Update User'}
          </Button>
        </form>
      </Container>
    </CLayout>
  );
}
