import { Container } from '@mui/material'
import React from 'react'
import Carousel from './carousel'

export const SamsungHero = () => {
    return (
        <Container maxWidth='xl' sx={{ px: { xs: 0, sm: '5%', md: 'inherit' } }}>
            <Carousel />
        </Container>
    )
}
