import { AddSharp, RemoveSharp } from "@mui/icons-material";
import { IconButton, TextField, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";

const QuantityController = ({ model }) => {
    return (
        <Box
            sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                border: "1px solid #70707050",
                borderRadius: .75,
                overflow: "hidden",
            }}
        >
            <IconButton sx={{ borderRadius: 0,borderRight: "1px solid #70707050",color: '#424242' }}>
                <RemoveSharp />
            </IconButton>
            <Typography variant="button" sx={{ minWidth: 42, textAlign: "center" }}>
                {model?.quantity}
            </Typography>
            <IconButton sx={{ borderRadius: 0,borderLeft: "1px solid #70707050",color: '#424242' }}>
                <AddSharp />
            </IconButton>
        </Box>
    );
};

export default QuantityController;
