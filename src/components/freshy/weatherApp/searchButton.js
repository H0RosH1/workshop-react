import React from 'react';
import Button from '@mui/material/Button';
import SearchIcon from '@mui/icons-material/Search';

const SearchButton = ({ onClick }) => {
  return (
    <Button
      className='w-full'
      variant="contained"
      color="primary"
      startIcon={<SearchIcon />}
      onClick={onClick}
    >
      Search
    </Button>
  );
};

export default SearchButton;
