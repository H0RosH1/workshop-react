// pages/login.js
import { useState, useEffect } from "react";
import { TextField, Button, Container, Typography } from "@mui/material";
import { useRouter } from "next/router";

const Profile = () => {
  const [userData, setUserData] = useState(null);
  const router = useRouter();
  useEffect(() => {
    // เรียกใช้ข้อมูลผู้ใช้งานเมื่อคอมโพเนนต์ถูกโหลดครั้งแรก
    const jwt = localStorage.getItem("jwt");

    // ตรวจสอบว่ามี JWT หรือไม่
    if (jwt) {
      fetchUserData(jwt);
    }
  }, []);

  const handleLogout = () => {
    // ลบข้อมูลผู้ใช้จาก localStorage
    localStorage.removeItem("jwt");
    localStorage.removeItem("user");

    // ลบ state ข้อมูลผู้ใช้ในแอพพลิเคชัน
    setUserData(null); // ตัวอย่างเท่านั้น ต้องได้รับ state นี้จาก props หรือ context

    // ทำการ redirect หรือเปลี่ยนเส้นทางไปยังหน้า Login
    router.push("/workShop/tong"); // ตัวอย่างเท่านั้น ต้องเปลี่ยนไปตามโครงสร้าง route ของคุณ
  };

  const fetchUserData = async (jwt) => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_AUTH_URL}/user`, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${jwt}`,
        },
      });

      if (!response.ok) {
        throw new Error("Failed to fetch user data");
      }

      const userData = await response.json();

      // แสดงข้อมูลผู้ใช้ใน console
      console.log("User Data:", userData);

      // บันทึกข้อมูลผู้ใช้ลงใน state
      setUserData(userData);

      // บันทึกข้อมูลผู้ใช้ลงใน Local Storage
      localStorage.setItem("user", JSON.stringify(userData));

      console.log("status:", userData.status);
      console.log("Name:", userData.user.fname, userData.user.lname);
    } catch (error) {
      // แสดง error ใน console
      console.error("Error fetching user data:", error.message);
    }
  };

  return (
    <Container maxWidth="sm">
      <Typography variant="h4" align="center" gutterBottom>
        Profile
      </Typography>

      {userData ? (
        <div style={{ textAlign: "center" }}>
          <img
            src={userData.user.avatar}
            alt="User Avatar"
            style={{ width: "100px", height: "100px", borderRadius: "50%", margin: "0 auto" }}
          />
          <br />
          <Typography variant="subtitle1">
            Name: {userData.user.fname} {userData.user.lname}
          </Typography>
          <Typography variant="subtitle1">Email: {userData.user.email}</Typography>
          {/* เพิ่มข้อมูลเพิ่มเติมตามต้องการ */}
          <br/>
          <Button variant="contained" color="primary" onClick={handleLogout}>
            Logout
          </Button>
        </div>
      ) : (
        <Typography variant="body1">Loading user data...</Typography>
      )}
    </Container>
  );
};

export default Profile;
