import React, { useState } from "react";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";

const ProductFilter = ({ onFilter }) => {
  const [searchTerm, setSearchTerm] = useState("");
  
  const handleSearch = () => {
    onFilter(searchTerm);
  };

  return (
    <Grid container spacing={2} justifyContent="center" marginBottom="30px">
      <Grid item xs={12} md={6}>
        <TextField
          fullWidth
          label="Search Product"
          variant="outlined"
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
      </Grid>
      <Grid item xs={12} md={2}>
        <Button
          variant="contained"
          color="primary"
          onClick={handleSearch}
          style={{ height: "100%" }}
        >
          Search
        </Button>
      </Grid>
    </Grid>
  );
};

export default ProductFilter;
