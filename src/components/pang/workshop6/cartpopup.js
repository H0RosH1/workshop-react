import React from "react";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogActions from "@mui/material/DialogActions";
import Button from "@mui/material/Button";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";



const CartPopup = ({ cartItems, onClose }) => {
  // คำนวณราคาสินค้ารวมทั้งส่วนลด
  const totalNet = cartItems.reduce(
    (total, item) => total + item.price * (1 - item.discountPercentage / 100),
    0
  );

  
  const totalPrice = cartItems.reduce((total, item) => total + item.price, 0);
  // คำนวณ total discount
  const totalDiscount = cartItems.reduce(
    (total, item) => total + item.price * (item.discountPercentage / 100),
    0
  );

  return (
    <Dialog
      open={true}
      onClose={onClose}
      PaperProps={{
        style: {
          position: "unset",
          top: 0,
          right: 0,
          width: "500px",
        },
      }}
    >
      <DialogTitle style={{ textAlign: "center" }}>ตะกร้าของคุณ</DialogTitle>
      <DialogContent>
        <List>
          {cartItems.map((item) => (
            <ListItem key={item.id}>
              <img
                src={item.picture}
                alt={item.Name}
                style={{ width: "120px", marginRight: "10px" }}
              />
              <ListItemText primary={`${item.Name}`} />
              <ListItemText
                style={{ textAlign: "right" }}
                primary={`${item.price}.-`}
                secondary={`${item.discountPercentage}%`}
              />
            </ListItem>
          ))}
        </List>
        <DialogContentText style={{ textAlign: "right" }}>
          จำนวนทั้งหมด: {cartItems.length} อย่าง
        </DialogContentText>
        <DialogContentText style={{ textAlign: "right" }}>
          ยอดรวม: {totalPrice}
        </DialogContentText>
        <DialogContentText style={{ textAlign: "right" }}>
          ส่วนลด: {totalDiscount}
        </DialogContentText>
        <DialogContentText style={{ textAlign: "right" }}>
          ยอดสุทธิ: {totalNet}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="primary">
          ปิด
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default CartPopup;
