import { Box, Grid, Avatar } from "@mui/material";
import React from "react";

const Body2 = () => {
  return (
    <div>
      <Grid container>
        {/* Single row with one column */}
        <Grid item style={{ display: "flex", justifyContent: "center", paddingLeft: "120px"}}>
          <Box
            style={{
              display: "flex",
              justifyContent: "space-around", // Adjust alignment as needed
              backgroundColor: "white",
              padding: 30,
              width: "1000px", // Set width to 100% for responsiveness
              
            }}
          >
            
            {/* First Avatar */}
            <Avatar
              alt="Profile Image"
              src="/tcc.jpg"
              sx={{ width: 75, height: 75, backgroundColor: "#000000" }}
            />

            {/* Second Avatar */}
            <Avatar
              alt="Profile Image"
              src="/tcc.jpg"
              sx={{ width: 75, height: 75, backgroundColor: "#000000" }}
            />

            {/* Third Avatar */}
            <Avatar
              alt="Profile Image"
              src="/tcc.jpg"
              sx={{ width: 75, height: 75, backgroundColor: "#000000" }}
            />

            {/* Fourth Avatar */}
            <Avatar
              alt="Profile Image"
              src="/tcc.jpg"
              sx={{ width: 75, height: 75, backgroundColor: "#000000" }}
            />
          </Box>
        </Grid>
      </Grid>
    </div>
  );
};

export default Body2;
