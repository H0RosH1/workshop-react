import React from "react";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import Link from "@mui/material/Link";
import Avatar from "@mui/material/Avatar";
import { TextField } from "@mui/material";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";

const Footer4 = () => {
  return (
    <Box bgcolor="#000000" color="#FFFFFF" paddingY={5} overflow="hidden" paddingTop={40}>
      <Container maxWidth="auto">
        <Grid container spacing={3} marginX={{ xs: 1, sm: 2, md: 5 }} zIndex={1}>
          <Grid item xs={12} md={6}>
            <div
              style={{
                fontSize: "20px",
                fontWeight: "bold",
                color: "#FFFFFF",
                textAlign: "left",
                letterSpacing: 5,
              }}
            >
              INFORMATION
            </div>
            <Typography
              style={{ fontSize: "10px", color: "#FFFFFF", textAlign: "left", marginTop: 10 }}
            >
              Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
            </Typography>
          </Grid>
          <Grid item xs={12} md={3}>
            <div
              style={{
                fontSize: "20px",
                fontWeight: "bold",
                color: "#FFFFFF",
                textAlign: "left",
                letterSpacing: 5,
              }}
            >
              NAVIGATION
            </div>
            <div
              style={{
                fontSize: "10px",
                fontWeight: "semibold",
                color: "#FFFFFF",
                textAlign: "left",
                marginTop: 10,
                letterSpacing: 1,
              }}
            >
              <ArrowForwardIosIcon style={{ width: "13px", height: "13px" }} />
              Homepage
            </div>
            <div
              style={{
                fontSize: "10px",
                fontWeight: "semibold",
                color: "#FFFFFF",
                textAlign: "left",
                marginTop: 10,
                letterSpacing: 1,
              }}
            >
              <ArrowForwardIosIcon style={{ width: "13px", height: "13px" }} />
              About us
            </div>
            <div
              style={{
                fontSize: "10px",
                fontWeight: "semibold",
                color: "#FFFFFF",
                textAlign: "left",
                marginTop: 10,
                letterSpacing: 1,
              }}
            >
              <ArrowForwardIosIcon style={{ width: "13px", height: "13px" }} />
              Services
            </div>
            <div
              style={{
                fontSize: "10px",
                fontWeight: "semibold",
                color: "#FFFFFF",
                textAlign: "left",
                marginTop: 10,
                letterSpacing: 1,
              }}
            >
              <ArrowForwardIosIcon style={{ width: "13px", height: "13px" }} />
              Project
            </div>
          </Grid>
          <Grid item xs={12} md={3}>
            <div
              style={{
                fontSize: "20px",
                fontWeight: "bold",
                color: "#FFFFFF",
                textAlign: "left",
                letterSpacing: 5,
              }}
            >
              CONTACT US
            </div>
            <div
              style={{
                fontSize: "10px",
                fontWeight: "semibold",
                color: "#FFFFFF",
                textAlign: "left",
                marginTop: 10,
                letterSpacing: 1,
              }}
            >
              Lumbung Hidup East Java
            </div>
            <div
              style={{
                fontSize: "10px",
                fontWeight: "semibold",
                color: "#FFFFFF",
                textAlign: "left",
                marginTop: 10,
                letterSpacing: 1,
              }}
            >
              Hello@homco.com
            </div>
            
          </Grid>
        </Grid>

        {/* Footer */}
        <Box marginTop={{ xs: 2, md: 5 }} textAlign="center">
          <Typography variant="body2">© 2023 Your Company</Typography>
        </Box>
      </Container>
    </Box>
  );
};

export default Footer4;
