// import React, { useState, useEffect } from "react";
// import Button from "@mui/material/Button";
// import Typography from "@mui/material/Typography";
// import Avatar from "@mui/material/Avatar";
// import Table from "@mui/material/Table";
// import TableBody from "@mui/material/TableBody";
// import TableCell from "@mui/material/TableCell";
// import TableContainer from "@mui/material/TableContainer";
// import TableHead from "@mui/material/TableHead";
// import TableRow from "@mui/material/TableRow";
// import Paper from "@mui/material/Paper";
// import TextField from "@mui/material/TextField";

// const UserListPagination = () => {
//   const [userList, setUserList] = useState([]);
//   const [currentPage, setCurrentPage] = useState(1);
//   const [perPage, setPerPage] = useState(10);

//   useEffect(() => {
//     fetchUserList();
//   }, [currentPage, perPage]);

//   const fetchUserList = () => {
//     fetch(`https://www.melivecode.com/api/users?page=${currentPage}&per_page=${perPage}`)
//       .then((response) => response.json())
//       .then((data) => {
//         console.log("เส้น 3:", data);
//           if (data && Array.isArray(data.data)) {
//           setUserList(data.data);
//           console.log(`Users API Response (page ${currentPage}, per_page ${perPage}):`, data.data);
//         } else {
//           console.error("Invalid data format. Expected an array.");
//         }
//       })
//       .catch((error) => console.error("Error fetching users:", error));
//   };
//   const handleNextPage = () => {
//     setCurrentPage(currentPage + 1);
//   };

//   const handlePrevPage = () => {
//     if (currentPage > 1) {
//       setCurrentPage(currentPage - 1);
//     }
//   };

//   return (
//     <div>
//       <Typography variant="h4">Workshop 5 - เส้น 3</Typography>

//       <TableContainer component={Paper}>
//         <Table>
//           <TableHead>
//             <TableRow>
//               <TableCell>Avatar</TableCell>
//               <TableCell>Name</TableCell>
//               <TableCell>Username</TableCell>
//             </TableRow>
//           </TableHead>
//           <TableBody>
//             {Array.isArray(userList) &&
//               userList.map((user) => (
//                 <TableRow key={user.id}>
//                   <TableCell>
//                     <Avatar alt={`${user.fname} ${user.lname}`} src={user.avatar} />
//                   </TableCell>
//                   <TableCell>{`${user.fname} ${user.lname}`}</TableCell>
//                   <TableCell>{user.username}</TableCell>
//                 </TableRow>
//               ))}
//           </TableBody>
//         </Table>
//       </TableContainer>

//       <div style={{ marginTop: "10px" }}>
//         <Button variant="contained" color="primary" onClick={handlePrevPage}>
//           Previous Page
//         </Button>
//         <span style={{ margin: "0 10px" }}>{`Page ${currentPage}`}</span>
//         <Button variant="contained" color="primary" onClick={handleNextPage}>
//           Next Page
//         </Button>
//       </div>
//     </div>
//   );
// };

// export default UserListPagination;
