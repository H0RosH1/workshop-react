import React, { useState } from "react";
import { Button, Typography, ThemeProvider } from "@mui/material";
import { createTheme } from "@mui/material/styles";

const Totalapi = () => {
  const [userList, setUserList] = useState([]);
  const [userSearch, setUserSearch] = useState([]);
  const [userPagination, setUserPagination] = useState([]);
  const [userSort, setUserSort] = useState([]);
  const [userlistAll, setUserlistAll] = useState([]);
  const [userDetail, setUserDetail] = useState([]);
  const apiUrl = process.env.NEXT_PUBLIC_API_URL;

  const theme = createTheme({
    palette: {
      primary: {
        main: "#00bcd4",
        light: "#42a5f5",
        dark: "#1565c0",
        contrastText: "#fff",
      },
    },
  });

  const fetchUsers = () => {
    fetch(`${apiUrl}/users`)
      .then((response) => response.json())
      .then((data) => {
        setUserList(data);
        console.log("Userlist เส้น1:", data);
      })
      .catch((error) => console.error("Error fetching users:", error));
  };

  const fetchSearch = () => {
    fetch(`${apiUrl}/users?search=karn`)
      .then((response) => response.json())
      .then((data) => {
        setUserSearch(data);
        console.log("Userlist(search) เส้น2:", data);
      })
      .catch((error) => console.error("Error Userlist(search):", error));
  };

  const fetchPagination = () => {
    fetch(`${apiUrl}/users?page=1&per_page=10`)
      .then((response) => response.json())
      .then((data) => {
        setUserPagination(data);
        console.log("Userlist(pagination) เส้น3:", data);
      })
      .catch((error) => console.error("Error Userlist(pagination):", error));
  };

  const fetchSort = () => {
    fetch(`${apiUrl}/users?sort_column=id&sort_order=desc`)
      .then((response) => response.json())
      .then((data) => {
        setUserSort(data);
        console.log("Userlist(sorted) เส้น4:", data);
      })
      .catch((error) => console.error("Error Userlist(sorted):", error));
  };

  const fetchAll = () => {
    fetch(`${apiUrl}/users?search=ka&page=1&per_page=10&sort_column=id&sort_order=desc`)
      .then((response) => response.json())
      .then((data) => {
        setUserlistAll(data);
        console.log("Userlist(all) เส้น5:", data);
      })
      .catch((error) => console.error("Error Userlist(all):", error));
  };

  const fetchDetail = () => {
    fetch(`${apiUrl}/users/1`)
      .then((response) => response.json())
      .then((data) => {
        setUserDetail(data);
        console.log("UserDetail เส้น6:", data);
      })
      .catch((error) => console.error("Error Userdetail:", error));
  };

  return (
    <ThemeProvider theme={theme}>
      <div
        style={{
          textAlign: "center",
          marginTop: "20px",
          display: "flex",
          flexDirection: "column",
        }}
      >
        <Typography variant="h4" mb="20px">
          Workshop 5 - User
        </Typography>

        <Button variant="contained" color="primary" onClick={fetchUsers} sx={{ margin: "10px" }}>
          USER LIST
        </Button>
        <Button variant="contained" color="primary" onClick={fetchSearch} sx={{ margin: "10px" }}>
          USER LIST (search)
        </Button>
        <Button
          variant="contained"
          color="primary"
          onClick={fetchPagination}
          sx={{ margin: "10px" }}
        >
          USER LIST (pagination)
        </Button>
        <Button variant="contained" color="primary" onClick={fetchSort} sx={{ margin: "10px" }}>
          USER LIST (sorted)
        </Button>
        <Button variant="contained" color="primary" onClick={fetchAll} sx={{ margin: "10px" }}>
          USER LIST (all)
        </Button>
        <Button variant="contained" color="primary" onClick={fetchDetail} sx={{ margin: "10px" }}>
          USER DETAIL
        </Button>
      </div>
    </ThemeProvider>
  );
};

export default Totalapi;
