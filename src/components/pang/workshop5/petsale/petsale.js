import React, { useState } from "react";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { createTheme, ThemeProvider } from "@mui/material/styles"; // Import ThemeProvider



const Petsale = () => {
  const [petSummary, setPetSummary] = useState([]); //เส้น20
  const [petDaily, setDaily] = useState([]); //เส้น21

  const apiUrl3 = process.env.NEXT_PUBLIC_API_URL;

  const theme = createTheme({
    palette: {
      primary: {
        main: "#00bcd4",
        light: "#42a5f5",
        dark: "#1565c0",
        contrastText: "#fff",
      },
    },
  });

  const fetchPetSummary = () => {
    fetch(`${apiUrl3}/pets/7days/2023-01-01`)
      .then((response) => response.json())
      .then((data) => {
        setPetSummary(data);
        console.log("setPetSummary เส้น19:", data);
      })
      .catch((error) => console.error("Error fetching users:", error));
  };

  const fetchPetDaily = () => {
    fetch(`${apiUrl3}/pets/2023-01-01`)
      .then((response) => response.json())
      .then((data) => {
        setDaily(data);
        console.log("setDaily เส้น20:", data);
      })
      .catch((error) => console.error("Error fetching users:", error));
  };

  return (
    <ThemeProvider theme={theme}>

    <div style={{
          textAlign: "center",
          marginTop: "20px",
          display: "flex",
          flexDirection: "column",
        }}>
      <Typography variant="h4" textAlign="center" mb="20px">
        Workshop 5 - Pet sale
      </Typography>
      <div>
        <Button
          variant="contained"
          color="primary"
          sx={{ ml: "10px" }}
          onClick={() => fetchPetSummary()}
        >
          PetSales 7days
        </Button>

        <Button
          variant="contained"
          color="primary"
          sx={{ ml: "10px" }}
          onClick={() => fetchPetDaily()}
        >
          Pet Daily
        </Button>
      </div>
    </div>
    </ThemeProvider>

  );
};

export default Petsale;
