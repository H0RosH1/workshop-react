import React, { useState } from "react";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { createTheme, ThemeProvider } from "@mui/material/styles"; // Import ThemeProvider


const Totalattractions = ({ handleButtonClick }) => {
  const [attractionList, setAttractionList] = useState([]); //เส้น10
  const [attractionSearch, setAttractionSearch] = useState([]); //เส้น11
  const [attractionPagination, setAttractionPagination] = useState([]); //เส้น12
  const [attractionSort, setAttractionSort] = useState([]); // เส้น13
  const [attrantionAll, setAttractionAll] = useState([]); //เส้น 14
  const [attrantionLanguage, setAttractionLanguage] = useState([]); //เส้น 15
  const [attrantionDetail, setAttrantionDetail] = useState([]); // เส้น 16
  const [attrantionDetailLanguage, setAttractionDetailLanguage] = useState([]); // เส้น 17
  const [attrantionStatic, setAttractionStatic] = useState([]); // เส้น 18
  const [attrantionButton, setActiveButton] = useState(null);

  const apiUrl2 = process.env.NEXT_PUBLIC_API_URL;

  const theme = createTheme({
    palette: {
      primary: {
        main: "#00bcd4",
        light: "#42a5f5",
        dark: "#1565c0",
        contrastText: "#fff",
      },
    },
  });

  const fetchAttractionList = () => {
    fetch(`${apiUrl2}/attractions`)
      .then((response) => response.json())
      .then((data) => {
        setAttractionList(data);
        console.log("Attractionlist เส้น10:", data);
      })
      .catch((error) => console.error("Error fetching users:", error));
  };

  const fetchAttractionSearch = () => {
    fetch(`${apiUrl2}/attractions?search=island`)
      .then((response) => response.json())
      .then((data) => {
        setAttractionSearch(data);
        console.log("AttractionSearch เส้น11:", data);
      })
      .catch((error) => console.error("Error fetching users:", error));
  };

  const fetchAttractionPagination = () => {
    fetch(`${apiUrl2}/attractions?page=1&per_page=10`)
      .then((response) => response.json())
      .then((data) => {
        setAttractionPagination(data);
        console.log("AttractionPagination เส้น12:", data);
      })
      .catch((error) => console.error("Error fetching users:", error));
  };

  const fetchAttractionSort = () => {
    fetch(`${apiUrl2}/attractions?sort_column=id&sort_order=desc`)
      .then((response) => response.json())
      .then((data) => {
        setAttractionSort(data);
        console.log("AttractionSort เส้น13:", data);
      })
      .catch((error) => console.error("Error fetching users:", error));
  };

  const fetchAttrantionAll = () => {
    fetch(`${apiUrl2}/attractions?search=island&page=1&per_page=10&sort_column=id&sort_order=desc`)
      .then((response) => response.json())
      .then((data) => {
        setAttractionAll(data);
        console.log("AttractionAll เส้น14:", data);
      })
      .catch((error) => console.error("Error fetching users:", error));
  };

  const fetchAtrantionLanguage = () => {
    fetch(`${apiUrl2}/th/attractions`)
      .then((response) => response.json())
      .then((data) => {
        setAttractionLanguage(data);
        console.log("AttractionLanguage เส้น15:", data);
      })
      .catch((error) => console.error("Error fetching users:", error));
  };

  const fetchAtrantionDetail = () => {
    fetch(`${apiUrl2}/attractions/1`)
      .then((response) => response.json())
      .then((data) => {
        setAttrantionDetail(data);
        console.log("AttractionDetail เส้น16:", data);
      })
      .catch((error) => console.error("Error fetching users:", error));
  };

  const fetchAtrantionDetailLanguage = () => {
    fetch(`${apiUrl2}/th/attractions/1`)
      .then((response) => response.json())
      .then((data) => {
        setAttractionDetailLanguage(data);
        console.log("setAttractionDetailLanguage เส้น17:", data);
      })
      .catch((error) => console.error("Error fetching users:", error));
  };

  const fetchAtrantionStatic = () => {
    fetch(`${apiUrl2}/attractions/static_paths`)
      .then((response) => response.json())
      .then((data) => {
        setAttractionStatic(data);
        console.log("setAttractionStatic เส้น18:", data);
      })
      .catch((error) => console.error("Error fetching users:", error));
  };

  return (
    <ThemeProvider theme={theme}>
      <div  style={{
          textAlign: "center",
          marginTop: "20px",
          display: "flex",
          flexDirection: "column",
        }}>
          
        <Typography variant="h4" textAlign="center">
          Workshop 5 - Attraction
        </Typography>

        <div>
          <Button
            variant="contained"
            color="primary"
            sx={{ margin: "10px" }}
            onClick={() => fetchAttractionList()}
          >
            Attraction List
          </Button>

          <Button
            variant="contained"
            color="primary"
            sx={{ margin: "10px" }}
            onClick={() => fetchAttractionSearch()}
          >
            Attraction Search
          </Button>

          <Button
            variant="contained"
            color="primary"
            sx={{ margin: "10px" }}
            onClick={() => fetchAttractionPagination()}
          >
            Attraction Pagination
          </Button>

          <Button
            variant="contained"
            color="primary"
            sx={{ margin: "10px" }}
            onClick={() => fetchAttractionSort()}
          >
            Attraction Sort
          </Button>

          <Button
            variant="contained"
            color="primary"
            sx={{ margin: "10px" }}
            onClick={() => fetchAttrantionAll()}
          >
            Attraction All
          </Button>

          <Button
            variant="contained"
            color="primary"
            sx={{ margin: "10px" }}
            onClick={() => fetchAtrantionLanguage()}
          >
            Attraction Language
          </Button>

          <Button
            variant="contained"
            color="primary"
            sx={{ margin: "10px" }}
            onClick={() => fetchAtrantionDetail()}
          >
            Attraction Detail
          </Button>

          <Button
            variant="contained"
            color="primary"
            sx={{ margin: "10px" }}
            onClick={() => fetchAtrantionDetailLanguage()}
          >
            Attractio DetailLanguage
          </Button>

          <Button
            variant="contained"
            color="primary"
            sx={{ margin: "10px" }}
            onClick={() => fetchAtrantionStatic()}
          >
            Attraction Static
          </Button>
        </div>
      </div>
    </ThemeProvider>
  );
};

export default Totalattractions;
