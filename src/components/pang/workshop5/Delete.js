import React, { useState } from "react";
import { Button, TextField, Typography } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";

const Delete = ({ onDelete }) => {
  const [userId, setUserId] = useState(""); 
  const [deleteLog, setDeletelog] = useState("");

  const theme = createTheme({
    palette: {
        primary: {
          main: "#00bcd4",
          light: "#42a5f5",
          dark: "#1565c0",
          contrastText: '#fff',
        },
      },
  });

  const handleDelete = async () => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/delete`, {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: userId,
        }),
      });

      const data = await response.json();

      if (response.ok) {
        console.log("ข้อมูลของเส้นที่ 9", "Delete", data);
        onDelete(data);
      } else {
        console.error("errorrr", "Delete failed:", data);
      }
    } catch (error) {
      console.error("errorrr", "An error occurred during delete:", error);
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <div style={{ display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "center"}}>
        <Typography variant="h6" gutterBottom>
          Delete Userrr
        </Typography>
        <TextField
          label="User ID"
          variant="outlined"
          fullWidth
          margin="normal"
          size="small"
          value={userId}
          onChange={(e) => setUserId(e.target.value)}
        />
        <Button variant="contained" color="primary" onClick={handleDelete}>
          Delete Userrrr
        </Button>
        {deleteLog && (
          <Typography variant="body2" color="error" gutterBottom>
            {deleteLog}
          </Typography>
        )}
      </div>
    </ThemeProvider>
  );
};

export default Delete;
