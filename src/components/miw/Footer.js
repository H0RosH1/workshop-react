import { Typography, Container, Link, Box } from "@mui/material";
import { IconButton } from "@mui/material";
import InstagramIcon from "@mui/icons-material/Instagram";

export default function Footer() {
  return (
    <footer>
      <Box
        sx={{
          bgcolor: "background.paper",
          py: 3,
          px: 2,
          mt: "auto",
        }}
      >
        <Container maxWidth="sm">
          <Typography variant="body2" color="text.secondary" align="center">
            <Link
              href="https://www.instagram.com/xx.hiyori/"
              target="_blank"
              rel="noopener noreferrer"
            >
              <IconButton>
                <InstagramIcon />
              </IconButton>
            </Link>
            © {new Date().getFullYear()} ChanoknanU
          </Typography>
        </Container>
      </Box>
    </footer>
  );
}
