import React from "react";
import { Box, Typography, Card, CardContent } from "@mui/material";

const Bodyui0 = () => {
  return (
    <>
      <div
        className="pt-16"
        style={{ display: "flex", flexDirection: "row", justifyContent: "center" }}
      >
        <Card sx={{ width: "auto", mx: "auto", backgroundColor: "", margin: "0 8px" }}>
          <CardContent style={{ padding: "24px" }}>
            <Box
              sx={{ width: "auto", margin: "auto", padding: "165px", backgroundColor: "#bdbdbd" }}
            ></Box>
            <Typography variant="h6" sx={{ color: "#455a64", fontWeight: "bold", pt: 5 }}>
              10 TRENDS HOME DECORATION <br />
              FOR BEST HOUSE
            </Typography>
            <Typography sx={{ color: "#455a64", paddingTop: "16px" }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor
              incididunt ut labore et dolore magna aliqua.
            </Typography>
          </CardContent>
        </Card>

        <Card sx={{ width: "auto", mx: "auto", backgroundColor: "", margin: "0 8px" }}>
          <CardContent style={{ padding: "24px" }}>
            <Box
              sx={{ width: "auto", margin: "auto", padding: "165px", backgroundColor: "#bdbdbd" }}
            ></Box>
            <Typography variant="h6" sx={{ color: "#455a64", fontWeight: "bold", pt: 5 }}>
              10 TRENDS HOME DECORATION <br />
              FOR BEST HOUSE
            </Typography>
            <Typography sx={{ color: "#455a64", paddingTop: "16px" }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor
              incididunt ut labore et dolore magna aliqua.
            </Typography>
          </CardContent>
        </Card>
        <Card sx={{ width: "auto", mx: "auto", backgroundColor: "", margin: "0 8px" }}>
          <CardContent style={{ padding: "24px" }}>
            <Box
              sx={{ width: "auto", margin: "auto", padding: "165px", backgroundColor: "#bdbdbd" }}
            ></Box>
            <Typography variant="h6" sx={{ color: "#455a64", fontWeight: "bold", pt: 5 }}>
              10 TRENDS HOME DECORATION <br />
              FOR BEST HOUSE
            </Typography>
            <Typography sx={{ color: "#455a64", paddingTop: "16px" }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor
              incididunt ut labore et dolore magna aliqua.
            </Typography>
          </CardContent>
        </Card>
      </div>

      <div
        className="pt-16"
        style={{ display: "flex", flexDirection: "row", justifyContent: "center" }}
      >
        <Card sx={{ width: "auto", mx: "auto", backgroundColor: "", margin: "0 8px" }}>
          <CardContent style={{ padding: "24px" }}>
            <Box
              sx={{ width: "auto", margin: "auto", padding: "165px", backgroundColor: "#bdbdbd" }}
            ></Box>
            <Typography variant="h6" sx={{ color: "#455a64", fontWeight: "bold", pt: 5 }}>
              10 TRENDS HOME DECORATION <br />
              FOR BEST HOUSE
            </Typography>
            <Typography sx={{ color: "#455a64", paddingTop: "16px" }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor
              incididunt ut labore et dolore magna aliqua.
            </Typography>
          </CardContent>
        </Card>

        <Card sx={{ width: "auto", mx: "auto", backgroundColor: "", margin: "0 8px" }}>
          <CardContent style={{ padding: "24px" }}>
            <Box
              sx={{ width: "auto", margin: "auto", padding: "165px", backgroundColor: "#bdbdbd" }}
            ></Box>
            <Typography variant="h6" sx={{ color: "#455a64", fontWeight: "bold", pt: 5 }}>
              10 TRENDS HOME DECORATION <br />
              FOR BEST HOUSE
            </Typography>
            <Typography sx={{ color: "#455a64", paddingTop: "16px" }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor
              incididunt ut labore et dolore magna aliqua.
            </Typography>
          </CardContent>
        </Card>
        <Card sx={{ width: "auto", mx: "auto", backgroundColor: "", margin: "0 8px" }}>
          <CardContent style={{ padding: "24px" }}>
            <Box
              sx={{ width: "auto", margin: "auto", padding: "165px", backgroundColor: "#bdbdbd" }}
            ></Box>
            <Typography variant="h6" sx={{ color: "#455a64", fontWeight: "bold", pt: 5 }}>
              10 TRENDS HOME DECORATION <br />
              FOR BEST HOUSE
            </Typography>
            <Typography sx={{ color: "#455a64", paddingTop: "16px" }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor
              incididunt ut labore et dolore magna aliqua.
            </Typography>
          </CardContent>
        </Card>
      </div>
    </>
  );
};

export default Bodyui0;
