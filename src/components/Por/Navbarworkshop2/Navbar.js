import * as React from "react";
import PropTypes from "prop-types";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import MenuIcon from "@mui/icons-material/Menu";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import MenuItem from "@mui/material/MenuItem";
import Menu from "@mui/material/Menu";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

const drawerWidth = 240;

function DrawerAppBar(props) {
  const { window } = props;
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState);
  };

  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: "center" }}>
      <Typography variant="h6" sx={{ my: 2 }}>
        MUI
      </Typography>
      <Divider />
    </Box>
  );

  const container = window !== undefined ? () => window().document.body : undefined;

  return (
    <div>
      <Box sx={{ display: "flex" }}>
        <CssBaseline />
        <AppBar
          component="nav"
          sx={{
            backgroundColor: "#A9A9A9",
            height: "100px",
            position: "fixed",
            zIndex: 1000,
          }}
        >
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="start"
              onClick={handleDrawerToggle}
              sx={{ mr: 2, display: { sm: "none" } }}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              variant="h6"
              component="div"
              sx={{
                flexGrow: 1,
                display: { xs: "none", sm: "block" },
                textAlign: "center",
              }}
            >
              <img
                src="https://i.ibb.co/cFQ93Kr/1.png"
                alt="Your Logo"
                style={{
                  width: "200px",
                  height: "auto",
                  marginLeft: "5em",
                  marginTop: "1em",
                }}
              />
            </Typography>
            <Box
              sx={{
                display: { xs: "none", sm: "block" },
                marginTop: "1.5em",
                marginRight: "3em",
              }}
            >
              <Button key="HOME" sx={{ color: "#fff" }}>
                HOME
              </Button>

              <Button key="ABOUT" sx={{ color: "#fff" }}>
                ABOUT US
              </Button>

              <Button key="SERVICES" sx={{ color: "#fff" }}>
                OUR SERVICES
              </Button>

              <Button key="OUR PROJECTS" sx={{ color: "#fff" }}>
                OUR PROJECTS <ExpandMoreIcon></ExpandMoreIcon>
              </Button>

              <Button key="PORTFOLIO" sx={{ color: "#fff" }}>
                PORTFOLIO
              </Button>

              <Button key="PAGES" sx={{ color: "#fff" }}>
                PAGES <ExpandMoreIcon></ExpandMoreIcon>
              </Button>

              <IconButton
                size="large"
                edge="start"
                color="inherit"
                aria-label="menu"
                sx={{ mr: 2 }}
              >
                <MenuIcon />
              </IconButton>
            </Box>
          </Toolbar>
        </AppBar>
        <nav>
          <Drawer
            container={container}
            variant="temporary"
            open={mobileOpen}
            onClose={handleDrawerToggle}
            ModalProps={{
              keepMounted: true,
            }}
            sx={{
              display: { xs: "block", sm: "none" },
              "& .MuiDrawer-paper": {
                boxSizing: "border-box",
                width: drawerWidth,
              },
            }}
          >
            {drawer}
          </Drawer>
        </nav>
      </Box>
      <Toolbar sx={{ height: "100px" }} />
    </div>
  );
}

DrawerAppBar.propTypes = {
  window: PropTypes.func,
};

export default DrawerAppBar;
